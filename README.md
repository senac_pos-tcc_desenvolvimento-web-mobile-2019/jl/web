<p align="center">
    <img  alt="Growth" title="#Growth" src="src/assets/bannerWebVersion.svg" />
</p>

<h1 align="center"> 
	Growth: AgroTreinamentos online 🌾 
</h1>
<p align="center"> 🌿 Plataforma de treinamentos online focado no Agro.  🌱 </p>

<img src="https://img.shields.io/badge/web-react-blue">
<img src="https://img.shields.io/badge/node-12.17.0-green">
<img src="https://img.shields.io/badge/backend-firebase-orange">
<img src="https://img.shields.io/badge/hosting-firebase%20Hosting-green">

<h3 align="center"> 
	🚧 Status do projeto🚧
</h3>

<p align="center">
  <img src="https://img.shields.io/badge/versão-1.0.0-green"> - production <br>
  <img src="https://img.shields.io/badge/versão-1.0.1-yellow"> - under construction
</p>

<h2> 💻 Acesse o projeto <h2>


<a href="https://growth-c1cc5.web.app/">
  <img src="https://img.shields.io/badge/Acessar%20Projeto%20-Growth%20web-green">
</a>


Tabela de conteúdos
=================
<!--ts-->
   * [Sobre o projeto](#-sobre-o-projeto)
   * [Principais benefícios](#-principais-benef%C3%ADcios)
   * [Funcionalidades](#%EF%B8%8F-funcionalidades-web)
   * [Layout](#-layout)
   * [Como executar o projeto](#-como-executar-o-projeto)
      * [Rodando o projeto](#-rodando-o-projeto)
   * [Tecnologias](#-tecnologias)
   * [Autores](#-autores)
   * [Licença](#-licença)
<!--te-->

## 📋 Sobre o projeto
O Growth é uma plataforma de treinamentos online pensada para atender o Agro. Composta por aplicativo mobile e ambiente web, proporciona um espaço para centralização de tutoriais sobre as ferramentas e auxilia no processo de replicação de treinamentos entre os usuários.

O projeto é composto por 3 repositórios que se complementam, abrangendo assim ambiente web e mobile. 

## 📊 Principais benefícios
Projeto desenvolvido tendo como principais benefícios:
- Disponibilidade
- Objetividade
- Agilidade
- Segurança
- Padronização
- Escalabilidade
---

## ⚙️ Funcionalidades web

- [x] Autenticação de usuários
  - [x] Esqueci senha
- [x] CRUD de usuários
  - [x] Autocadastro de usuários
  - [x] Edição de Perfil
  - [x] Edição de papeis de usuário (Cliente, Funcionário e Administrador)
  - [x] Inativação de usuários
- [x] CRUD Treinamentos
  - [x] Criação de treinamentos com textos e midia (imagem ou vídeo)
  - [x] Edição de treinamentos
  - [x] Exclusão de treinamentos 
  - [x] Listagem e filtro de treinamentos 
- [x] CRUD Trilhas
  - [x] Criação de trilhas com treinamentos
  - [x] Edição de trilhas
  - [x] Exclusão de trilhas 
  - [x] Listagem e filtro de trilhas 
- [x] Dashboard
- [x] Acessar, iniciar e finalizar Treinamentos
- [x] Acessar, iniciar e finalizar Treinamentos
- [x] Pesquisar treinamentos
- [x] Pesquisar Trilhas


## 🎨 Layout

O layout da aplicação está disponível no Figma:

<a href="https://www.figma.com/file/yzMVxwHec0R7zt14i1Qu8c/Web?node-id=0%3A1">
  <img alt="Made by tgmarinho" src="https://img.shields.io/badge/Acessar%20Layout%20-Figma-%2304D361">
</a>

## 📦 Como executar o projeto

####  🛠  Pré Requisitos

Importante ter configurado ambiente de desenvolvimento com a versão atualizada do <kbd>NodeJS<kdb>.

💡 Para realizar deploy no Firebase hosting será importante seguir os passos definidos na documentação oficial.

#### 🎲 Rodando o projeto


```bash

# Clone este repositório
$ git clone git@gitlab.com:senac_pos-tcc_desenvolvimento-web-mobile-2019/jl/web.git

# Acesse a pasta do projeto no terminal/cmd
$ cd web

# Instale as dependências
$ yarn install

# Execute a aplicação em modo de desenvolvimento
$ yarn start

# A aplicação será aberta na porta:3000 - acesse http://localhost:3000

```


## 🕹 Tecnologias

As seguintes ferramentas foram usadas na construção do projeto web:

#### **Website**  ([React](https://reactjs.org/)  +  [Firebase](https://firebase.google.com/))

-   **[React Router Dom](https://github.com/ReactTraining/react-router/tree/master/packages/react-router-dom)**
-   **[React Router](https://reactrouter.com/)**
-   **[FontAwesome](https://fontawesome.com/)**
-   **[React i18next](https://react.i18next.com/)**


## 🦸 Autores

<table>
  <tr>
    <td align="center"><a href="https://gitlab.com/lekkinhah"><img style="border-radius: 50%;" src="https://pt.gravatar.com/userimage/186334662/ec308d4832e83fdc97fbb724d6f69a70.jpg" width="100px;" alt=""/><br /><sub><b>Letícia Vargas</b></sub></a><br /> </td>
    <td align="center"><a href="https://gitlab.com/jeferson.oliveira"><img style="border-radius: 50%;" src="https://pt.gravatar.com/userimage/16093557/f396bcb89e84b8e858ff92b5c5af91fd.jpeg" width="100px;" alt=""/><br /><sub><b>Jeferson Oliveira</b></sub></a><br /></td>
    
  </tr>

</table>


## 📝 Licença

Este projeto esta sobe a licença [MIT](./LICENSE).

Feito com ❤️ por Letícia Vargas e Jeferson Oliveira 👋🏽

---