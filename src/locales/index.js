import i18n from 'i18next';
import { initReactI18next, useTranslation } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';

import ptBR from './pt_BR';
import es from './es';
import en from './en';


let currentLanguage;

const getCurrentLanguage = () => currentLanguage;

/*const languageDetector = {
  type: 'languageDetector',
  async: true,
  detect: async (callback) => {
    currentLanguage = await AsyncStorage.getItem('current-language');
    if (!currentLanguage)
      currentLanguage = Platform.select({
        android: NativeModules.I18nManager.localeIdentifier,
        ios: (NativeModules.SettingsManager.settings.AppleLocale || NativeModules.SettingsManager.settings.AppleLanguages[0]),
      }).replace('_', '-');

    return callback(currentLanguage);
  },
  init: () => {},
  cacheUserLanguage: (language) => {
    currentLanguage = language;
    AsyncStorage.setItem('current-language', language)
  },
};*/

const options = {
  resources: {
    'pt-BR': ptBR,
    'es': es,
    'en': en
  },
  fallbackLng: 'pt-BR',
  interpolation: {
    escapeValue: false,
  },
};

export default i18n
 // .use(languageDetector)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init(options);

export {
  useTranslation,
  getCurrentLanguage
};
