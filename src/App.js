import React from 'react';

import { ContextProvider, useContextValue } from '@growth/commons/context';

import Routes from './routes';


function App() {
  const contextValue = useContextValue();

  return (
    <ContextProvider value={contextValue}>
      <Routes />
    </ContextProvider>
   );
}

export default App;
