import React from 'react';
import { useTranslation } from "react-i18next";
import { useHistory } from 'react-router-dom';
import {PageLayout, DashProgressCard, CourseCardMini, 
        TrackCardMini, TitleBottomLine} from '../../components';
import { usePublishedCourses, usePublishedTracks, useCurrentUser } from '@growth/commons/context';
import './style.css';

const Dashboard = () => {
  const {t} = useTranslation();
  const allCourses = usePublishedCourses();
  const allTracks = usePublishedTracks();
  const user = useCurrentUser();
  const listCourses = allCourses.slice(0,4);
  const listTracks = allTracks.slice(0,4);
  const history = useHistory();

  return(
    <PageLayout title={t('menu.dashboard')}>
      <div className="dashHeader">
        <div className="dashWellcomeContainer">
          <span className="title">{t("info.hello")}, {user.name}!</span>
          <span className="text">{t("text.goodToSeeYou")}</span>
        </div>
        <DashProgressCard />
      </div>
      <TitleBottomLine title={t("title.discoverNewCourses")} />
      <div className="dashBody">
        {
          listCourses.map((course)=> {return (<CourseCardMini key={course.id} course={course} onClick={() => history.push('/courses/' + course.id)} />)})
        }
      </div>
      <TitleBottomLine title={t("title.discoverNewTracks")} />
      <div className="dashBody">
        {
          listTracks.map((track)=> {return (<TrackCardMini key={track.id} track={track} onClick={() => history.push('/tracks/' + track.id)} />)})
        }
      </div>
    </PageLayout>
  );
};

export {Dashboard};