import React, { useState, useCallback } from 'react';
import { useTranslation } from "react-i18next";
import { Link } from 'react-router-dom';
import { signUp } from '@growth/commons/auth';
import { Button, TextInput, InputCheckBox} from '../../components';

import AuthForm from './authForm';
import './style.css';


const CreateAccount = () => {
	const {t} = useTranslation();

	const [name, setName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [showPwd, setShowPwd] = useState(false);
	const [terms, setTerms] = useState(false);
	const [loading, setLoading] = useState(false);
	const [error, setError] = useState(false);

	const handleNameChange = useCallback((e) => setName(e.target.value), []);
	const handleEmailChange = useCallback((e) => setEmail(e.target.value), []);
	const handlePasswordChange = useCallback((e) => setPassword(e.target.value), []);
	const handleTermsChange = useCallback((e) => setTerms(e.target.checked), []);
	const handleErrorConfirm = useCallback(() => setError(), []);
	const handleShowPwd = useCallback(() => setShowPwd(!showPwd), [showPwd]);

	const attemptSignUp = useCallback((e) => {
		e.preventDefault();

		if (!terms) {
			setError(t('auth.error.terms-must-be-accepted'));
			return;
		}

		setLoading(true);
		signUp(email, password, name).catch((error) => {
			setLoading(false);
			setError(t(error.code.replace('auth/', 'auth.error.')));
		});
	}, [email, password, name, terms]);

	return(
		<AuthForm 
			loading={loading}
			onSubmit={attemptSignUp}
			error={error}
			onErrorConfirm={handleErrorConfirm}
		>
			<TextInput
				name="userName"
				className="formInput"
				value={name}
				leftIcon="user" leftIconType="regular" 
				leftIconSize="1x" leftIconColor="var(--color-primary)"
				placeholder={t('input.name')}
				onChange={handleNameChange}
			/>

			<TextInput
				name="userEmail"
				className="formInput"
				value={email}
				leftIcon="envelope" leftIconType="regular"
				leftIconSize="1x" leftIconColor="var(--color-primary)"
				placeholder={t('input.email')}
				onChange={handleEmailChange}
			/>

			<TextInput
				name="userPwd"
				className="formInput"
				value={password}
				leftIcon="lock" leftIconType="solid" 
				leftIconSize="1x" leftIconColor="var(--color-primary)"
				placeholder={t('input.password')}
				type={showPwd ? "text" : "password"}
				rightIcon={showPwd ? "eye-slash" : "eye"} rightIconType="solid"
				rightIconSize="1x" rightIconColor="var(--color-black-light)"
				onRightIconClick={handleShowPwd}
				onChange={handlePasswordChange}
			/>

			<InputCheckBox
				id="terms"
				className="termOfUse"
				title={t('text.termOfUse')}
				checked={terms}
				onChange={handleTermsChange}
			/>

			<Button className="formButton" title={t('button.register')}/>
			<Link to="/signIn">
				<span>{t('link.alreadyMember')}</span>
				<span className="highlight">{t('link.doLogin')}</span>
			</Link>
		</AuthForm>
	);
}

export { CreateAccount };