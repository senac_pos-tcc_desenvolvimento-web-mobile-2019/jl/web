import React, { useState, useCallback, useEffect } from 'react';
import { BrandIcon, BrandText, ScreenLoader, AlertModal } from '../../components';
import './style.css';


const AuthForm = ({ onSubmit, loading, error, onErrorConfirm, children }) => {
  return (
    <div className="authContainer">
      <div className="brandContainer">
        <BrandIcon className='brandIconStyle' />
        <BrandText className='brandTextStyle' />
      </div>

      <form onSubmit={onSubmit} className="formContainer">
        { children }
      </form>

      <ScreenLoader visible={loading} />

      <AlertModal
        visible={error && error.length > 0}
        title="Houve um problema..."
        text={error}
        confirmTitle="OK"
        onConfirm={onErrorConfirm}
      />
    </div>
  );
};

export default AuthForm;