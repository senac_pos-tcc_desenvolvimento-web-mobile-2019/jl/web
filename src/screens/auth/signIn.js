import React, { useState, useCallback } from 'react';
import { useTranslation } from "react-i18next";
import { Link } from 'react-router-dom';
import { signIn } from '@growth/commons/auth';
import { Button, TextInput } from '../../components';

import AuthForm from './authForm';
import './style.css';


const SignIn = () => {
	const {t} = useTranslation();

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [showPwd, setShowPwd] = useState(false);
	const [loading, setLoading] = useState(false);
	const [error, setError] = useState(false);

	const handleEmailChange = useCallback((e) => setEmail(e.target.value), []);
	const handlePasswordChange = useCallback((e) => setPassword(e.target.value), []);
	const handleErrorConfirm = useCallback(() => setError(), []);
	const handleShowPwd = useCallback(() => setShowPwd(!showPwd), [showPwd]);

	const attemptSignIn = useCallback((e) => {
		e.preventDefault();
		setLoading(true);
		signIn(email, password).catch((error) => {
			setLoading(false);
			setError(t(error.code.replace('auth/', 'auth.error.')));
		});
	}, [email, password]);

	return(
		<AuthForm 
			loading={loading}
			onSubmit={attemptSignIn}
			error={error}
			onErrorConfirm={handleErrorConfirm}
		>
			<TextInput
				name="userEmail"
				className="formInput"
				value={email}
				leftIcon="envelope" leftIconType="regular"
				leftIconSize="1x" leftIconColor="var(--color-primary)"
				placeholder={t('input.email')}
				onChange={handleEmailChange}
			/>

			<TextInput
				name="userPwd"
				className="formInput"
				value={password}
				leftIcon="lock" leftIconType="solid" 
				leftIconSize="1x" leftIconColor="var(--color-primary)"
				placeholder={t('input.password')}
				type={showPwd ? "text" : "password"}
				rightIcon={showPwd ? "eye-slash" : "eye"} rightIconType="solid"
				rightIconSize="1x" rightIconColor="var(--color-black-light)"
				onRightIconClick={handleShowPwd}
				onChange={handlePasswordChange}
			/>

			<Link className="forgotPwd" to="/forgotPwd" >
				{t('link.forgotPwd')}
			</Link>

			<Button className="formButton" title={t('button.login')}/>
			<Link to="/createAccount">
				<span>{t('link.newAround')}</span>
				<span className="highlight">{t('link.register')}</span>
			</Link>
		</AuthForm>
	);

}

export { SignIn };