import React, { useState, useCallback } from 'react';
import { useTranslation } from "react-i18next";
import { resetPassword } from '@growth/commons/auth';
import { Button, TextInput, AlertModal } from '../../components';

import AuthForm from './authForm';
import './style.css';


const ForgotPwd = ({ history }) => {
	const {t} = useTranslation();

	const [email, setEmail] = useState('');
	const [loading, setLoading] = useState(false);
	const [error, setError] = useState(false);
	const [success, setSuccess] = useState(false);

	const handleEmailChange = useCallback((e) => setEmail(e.target.value), []);
	const handleErrorConfirm = useCallback(() => setError(), []);

	const goToSignIn = useCallback(() => history.push('/signIn'), []);

	const sendResetEmail = useCallback((event) => {
		event.preventDefault();
		setLoading(true);
		resetPassword(email)
		.then(() => {
			setLoading(false);
			setSuccess(true);
		})
		.catch((error) => {
			setLoading(false);
			setError(t(error.code.replace('auth/', 'auth.error.')));
		});
	}, [email]);

	return(
		<React.Fragment>
			<AuthForm
				loading={loading}
				onSubmit={sendResetEmail}
				error={error}
				onErrorConfirm={handleErrorConfirm}
			>
				<h2>{t('link.forgotPwd')}</h2>
				<p>{t('text.forgotPwd')}</p>

				<TextInput
					name="userEmail"
					value={email}
					leftIcon="envelope" leftIconType="regular"
					leftIconSize="1x" leftIconColor="var(--color-primary)"
					placeholder={t('input.email')}
					onChange={handleEmailChange}
				/>

				<Button className="formButton" title={t('button.redefine')}/>
			</AuthForm>

			<AlertModal
				visible={success}
				title="Sucesso!"
				text={t('auth.success.reset-email-sent')}
				confirmTitle="OK"
				onConfirm={goToSignIn}
			/>
		</React.Fragment>
	);
}

export  {ForgotPwd};