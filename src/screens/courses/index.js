import React, { useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import { Strings } from '@growth/commons/utils';
import { usePublishedCourses, useUserCourses } from '@growth/commons/context';

import { PageLayout, CourseCard, SearchBar, Pagination } from '../../components';
import './style.css';

const Courses = () => {

  const {t} = useTranslation();
  const allCourses = usePublishedCourses();
  const userCourses = useUserCourses();
  const history = useHistory();
  const location = useLocation();
  
  const [selectedButton, setSelectedButton] = useState(location.state || 'all');
  const [search, setSearch] = useState('');

  const availableList = selectedButton == 'all' ? allCourses : userCourses;
  const seachedList = !search.trim().length ? availableList : availableList.filter(course => {
    return Strings.containsSearch(course.name, search);
  });

  useEffect(() => {
    setSelectedButton(location.state || 'all');
  }, [location.state]);

  const renderCoursesContainer = (courses) => {
    return (<div className="coursesListCards">{courses}</div>)
  };

  const renderCourse = (course) => {
    return(
      <CourseCard
        key={course.id}
        course={course}
        onClick={() => history.push('/courses/' + course.id)}
      />
    );
  };

  return (
    <PageLayout title={t('menu.courses')}>
      <SearchBar
        legend={t('info.showMe') + ':'}
        buttons={[
          { title: t('button.allCourses'), onClick: () => setSelectedButton('all'), selected: selectedButton == 'all' },
          { title: t('button.myCourses'), onClick: () => setSelectedButton('my'), selected: selectedButton == 'my' }
        ]}
        search={search}
        onSearchChange={(e) => setSearch(e.target.value)}
      />
      <Pagination
        records={seachedList}
        recordsPerPage={4}
        renderRecordsContainer={renderCoursesContainer}
        renderRecord={renderCourse}
        emptyMessage="Nenhum treinamento encontrado"
      />
    </PageLayout>
  );
}

export {Courses};

