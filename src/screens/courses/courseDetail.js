import React, { useState, useEffect } from 'react';
import { useTranslation } from "react-i18next";
import { useParams, Redirect } from 'react-router-dom';

import { usePublishedCourses, useUserCourseProgress, useCurrentUser } from '@growth/commons/context';

import badge from '../../assets/badgeGreen.png';
import { PageLayout, PageHeader, PageFooter, Icon, Button, AlertModal, StarRating } from '../../components';
import './detailStyle.css';


const Course = () => {
  const { id } = useParams();
  const course = usePublishedCourses(courses => courses.find(course => course.id == id));
console.log("course image", course.imageUrl);
  if (!course)
    return (<Redirect to="/courses" />);
  else
    return (<CoursePage course={course} />);
};

const CoursePage = ({ course }) => {
  const {t} = useTranslation();
  const user = useCurrentUser();
  const progress = useUserCourseProgress(course);

  const [loading, setLoading] = useState(false);
  const [rating, setRating] = useState(progress && progress.ratedAs);
  const [finishedModalVisible, setFinishedModalVisible] = useState(false);

  useEffect(() => {
    user.setCourseAccess(course.id);
  }, []);

  const handleButonClick = (e) => {
    e.preventDefault();
    setLoading(true);

    if (!progress || !progress.status)
      user.setCourseProgress(course.id, 'pending').then(() => setLoading(false));
    else if (progress.status == 'pending')
      user.setCourseProgress(course.id, 'done').then(() => {
        setLoading(false);
        setFinishedModalVisible(true);
      });
    else if (progress.status == 'done')
      user.deleteCourseProgress(course.id).then(() => setLoading(false));
    else
      setLoading(false);
  };

  const handleEvaluationClick = (value) => {
    setRating(value);
  };

  const handleEvaluationCancel = () => {
    setRating();
    setFinishedModalVisible(false);
  };

  const handleEvaluationConfirm = () => {
    user.setCourseRating(course.id, rating);
    setFinishedModalVisible(false);
  };

  return (
    <PageLayout
      loading={loading}
      title={t('menu.courses')}
      renderHeader={() => (
        <PageHeader badge={badge} title={course.name} description={course.description}>
          <div className="courseDetailContainer">
            <div className="columnContainer">
              <div className="lineWrapper">
                <Icon name="clock" type="regular" size="1x" color="var(--color-gray)" />
                <div className="lineDetail">
                    <span className="courseTitleDetail">{t('info.estimatedTime')}</span>
                    <span className="courseInfoDetail"> {course.estimatedTime} {t("info.minutes")} </span>
                </div>
              </div>
              <div className="lineWrapper">
                <Icon name="star-half-alt" size="1x" color="var(--color-gray)" />
                <div className="lineDetail">
                    <span className="courseTitleDetail">{t('info.evaluation')}</span>
                    <span className="courseInfoDetail">
                      {course.rating ? parseFloat((course.rating.sum/course.rating.count).toFixed(1)).toLocaleString() : '-'}
                    </span>
                </div>
              </div>
            </div>
            <div className="columnContainer">
              <div className="lineWrapper">
                <Icon name="sync" size="1x" color="var(--color-gray)" />
                <div className="lineDetail">
                    <span className="courseTitleDetail">{t('info.updated')} </span>
                    <span className="courseInfoDetail"> {course.date.toLocaleDateString()} </span>
                </div>
              </div>
            </div>
          </div>
        </PageHeader>
      )}
      renderFooter={() =>(
        <PageFooter>
          <div className="courseBodyButton">
            <Button title={(!progress || !progress.status) ? 'Iniciar' : progress.status == 'pending' ? t("button.finishCourse") : 'Limpar'} onClick={handleButonClick} />
          </div>
        </PageFooter>
      )}
    >
      <div className="courseDetailBodyContent">
       
        {course.videoUrl && course.videoUrl.length != 0 &&
          <div className="courseDetailMediaWrapper">
            <iframe width="560" height="315" src={course.videoUrl} frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
          </div>
        }
        {course.imageUrl && (
          <div className="courseDetailMediaWrapper">
            <div className="courseDetailImage" style={{backgroundImage: `url(${course.imageUrl})`}}></div>     
          </div>
        )}
        <div className="courseDetailbodyText">
          {course.text}
        </div>
      </div>
      
      <AlertModal
        visible={finishedModalVisible}
        title={t('title.evaluation')}
        text={t('text.evaluation')}
        confirmTitle={t('alert.action.evaluation')}
        confirmDisabled={!rating}
        onConfirm={handleEvaluationConfirm}
        cancelTitle={t('alert.action.cancel')}
        onCancel={handleEvaluationCancel}
      >
        <span className="modalEvaluationLegend">{t('text.evaluationLegend')}</span>
        <div className="modalEvaluationStars">
          <StarRating value={rating} onClick={handleEvaluationClick}/>
        </div>
      </AlertModal>
    </PageLayout>
  );
}

export {Course};

