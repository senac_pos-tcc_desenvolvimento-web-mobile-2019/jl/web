//Auth screens
export {SignIn} from './auth/signIn';
export {ForgotPwd} from './auth/forgotPwd';
export {CreateAccount} from './auth/createAccount';


//Common screens
export {Profile} from './profile';
export {Courses} from './courses';
export {Course} from './courses/courseDetail';
export {Dashboard} from './dashboard';
export {Tracks} from './tracks';
export {Track} from './tracks/trackDetail';
export {Error} from './Error.js';


//Admin Screens
export {AdminTracks} from './admin/tracks';
export {ManageTrack} from './admin/tracks/manage';
export {AdminUsers} from './admin/users';
export {AdminCourses} from './admin/courses';
export {ManageCourse} from './admin/courses/manage';

export {Framework} from './admin/framework';