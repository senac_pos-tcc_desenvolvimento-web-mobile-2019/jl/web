import React, { useState, useCallback, useRef, useEffect } from 'react';
import firebase from 'firebase';
import { useCurrentUser} from '@growth/commons/context';

import { useTranslation, getCurrentLanguage } from '../../locales';
import { PageLayout, TitleBottomLine, PageFooter, 
         Button, TextInput, Dropzone, AlertModal} from '../../components';
import {phoneMask, dateMask} from '../../utils';
import './style.css';

const validateUserData = (data) => {
  const errors = {};

  if (!(data.name && data.name.length))
    errors.name = true;

  errors.count = Object.keys(errors).length;
  return errors;
};



const Profile = () => {

  const {t, i18n} = useTranslation();
  const options = {year: 'numeric', month: 'long', day: 'numeric' };
  const dropzone = useRef();
  const pageLayout = useRef();

  const user = useCurrentUser();
  
  const [loading, setLoading] = useState(false);
  const [errorModalVisible, setErrorModalVisible] = useState(false);
  const [pwdModalVisible, setPwdModalVisible] = useState(false);
  const [modalAlertSucess, setModalAlertSucess] = useState(false);
  const [errors, setErrors] = useState({ });
  const [avatarUrl, setAvatarUrl] = useState((user && user.avatarUrl) || '');
  const [name, setName] = useState((user && user.name) || '');
  const [phone, setPhone] = useState((user && user.phone) || '');
  const [birthday, setBirthday] = useState((user && user.birthday) || '');
  const [city, setCity] = useState((user && user.city) || '');
  const [pwd, setPwd] = useState('');
  const [pwdNew, setPwdNew] = useState('');
  const [pwdConfirm, setPwdConfirm] = useState('');
  const [showPwd, setShowPwd] = useState(false);
  const [showPwdNew, setShowPwdNew] = useState(false);
  const [showPwdConfirm, setShowPwdConfirm] = useState(false);

  const handleShowPwd = useCallback(() => setShowPwd(!showPwd), [showPwd]);
  const handleShowPwdNew = useCallback(() => setShowPwdNew(!showPwdNew), [showPwdNew]);
  const handleShowPwdConfirm = useCallback(() => setShowPwdConfirm(!showPwdConfirm), [showPwdConfirm]);

  const handlePhone = (e) =>{setPhone(phoneMask(e))};
  const handleBirthday =(e)=>{setBirthday(dateMask(e))};

  const handleAvatarError = (error) => {
    console.log('SALTA UM ERRO PRA ', error);
  };

  const handleAvatarUpload = async () => {
    if (dropzone.current) {
      const upAvatarUrl = await dropzone.current.upload('users/avatar/' + user.id);
      return upAvatarUrl;
    }
  };

  const handleCancelPwd = () => {
    setPwdModalVisible(false); 
    setErrors({}); 
    setPwd('');
    setPwdNew('');
    setPwdConfirm('');
    setShowPwdNew(false);
    setShowPwdConfirm(false);
    setShowPwd(false);
  };
  const handleSavePwd = () => {
    
    console.log("[Profile] ReauthenticatewithCredential");
    const errors = {};
    if(pwdNew !== pwdConfirm){
      errors.pwdStatus = "auth.error.notEqual-password";
      errors.pwdNew = true;
      errors.pwdConfirm = true;
      setErrors(errors);
      return;
    }

    let credential = firebase.auth.EmailAuthProvider.credential(
      user.email.trim(),
      pwd
    ); 
    user.__authUser.reauthenticateWithCredential(credential).then(() => {
      console.log("[Profile] Alterando a senha do usuário");
      user.__authUser.updatePassword(pwdNew).then(() => {
          console.log("[Profile] Senha alterada >|");
          handleCancelPwd();
          setModalAlertSucess(true);
        }).catch((error) => {
            console.log(error);
            errors.pwdStatus = error.code.replace('auth/', 'auth.error.');
            errors.pwdNew = true;
            errors.pwd = true;
            errors.pwdConfirm = true;
            setErrors(errors);
          });
    }).catch((error)=>{
      console.log(error);  
        errors.pwdStatus = error.code.replace('auth/', 'auth.error.');
        errors.pwd = true;
        setErrors(errors);
      });
  };

  const handleSave = () => {
    
    setLoading(true);

    const data = { name, phone, birthday, city};
    const errors = validateUserData(data);
    if (errors.count) {
      setErrors(errors);
      setErrorModalVisible(true);
      setLoading(false);
    } else {
      setErrors({ }); 
    
      handleAvatarUpload().then((upAvatarUrl) =>{
        if (user.avatarUrl != upAvatarUrl)
          data.avatarUrl = upAvatarUrl ? upAvatarUrl : null;
          user.update(data).then(() => {
          setLoading(false);
          setModalAlertSucess(true);
          });
      });
    }
  };
  
  return(
    <PageLayout 
      ref={pageLayout}
      loading={loading}
      title={t('menu.myProfile')} 
      renderFooter={() =>(
        <PageFooter>
            <Button title={t("button.finishEdit")} onClick={handleSave} />
        </PageFooter>
      )}
    >
      <div className="profileHeader">
        <div className="profileInfo">
          <Dropzone
            ref={dropzone}
            placeholder={t("profile.info.uploadAvatar")}
            type={Dropzone.IMAGE}
            previousFileUrl={avatarUrl}
            onValidationError={handleAvatarError}
            className="profilePhoto"
          />

          <div className="profileTitle">
            <span className="name">{user && user.name}</span>
            <span className="email">{user && user.email}</span>
            <span> {t("info.memberSince")} {user.createdAt && user.createdAt.toLocaleDateString(i18n.language, options)}</span>
          </div>
        </div>

        <div className="profileLanguage">
          <span className={i18n.language == 'pt-BR' ? 'selected' : ''} onClick={() => i18n.changeLanguage('pt-BR')}>Português</span>
          <span className={i18n.language == 'es' ? 'selected' : ''} onClick={() => i18n.changeLanguage('es')}>Español</span>
          <span className={i18n.language == 'en' ? 'selected' : ''} onClick={() => i18n.changeLanguage('en')}>English</span>
        </div>
      </div>

      <TitleBottomLine title = {t("title.profileInfo")} />

      <div className="profileBody">
        <div className="editProfile">
          <TextInput 
            className={errors.name ? 'error' : ''}
            leftIcon="user"
            leftIconType="regular"
            leftIconColor="var(--color-primary)"
            placeholder={t('input.name')} 
            type="text"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <TextInput 
            leftIcon="phone"
            leftIconColor="var(--color-primary)"
            placeholder={t('input.phone')}
            className="inputs" 
            type="text"
            maxlenght={12}
            value={phone}
            onChange={(e) => handlePhone(e.target.value)}
            
          />
          <TextInput 
            leftIcon="birthday-cake"
            leftIconColor="var(--color-primary)"
            placeholder={t('input.birthday')}
            className="inputs" 
            type="text"
            maxlenght={10}
            value={birthday}
            onChange={(e) => handleBirthday(e.target.value)}
          />
          <TextInput 
            leftIcon="map-marker-alt"
            leftIconColor="var(--color-primary)"
            placeholder={t('input.city')}
            className="inputs" 
            type="text"
            value={city}
            onChange={(e) => setCity(e.target.value)}
          />
        </div>
        <div className="editPwd">
          <Button title={t("button.editPwd")} className="square" onClick={() => {setPwdModalVisible(true)}}/>
        </div>
      </div>
      <AlertModal
        visible={errorModalVisible}
        title={t("profile.info.errorTitle")}
        confirmTitle={t('alert.action.ok')}
        onConfirm={() => setErrorModalVisible(false)}
      >
        <p>
          {t("profile.info.errorText")}
        </p>
      </AlertModal>
      <AlertModal
        visible={modalAlertSucess}
        title={t("profile.info.sucessTitle")}
        confirmTitle={t('alert.action.ok')}
        onConfirm={() => setModalAlertSucess(false)}
      >
        <p>
          {t("profile.info.sucessText")}
        </p>
      </AlertModal>
      <AlertModal
        visible={pwdModalVisible}
        title={t("profile.info.updatePwd")}
        text={t('profile.info.updatePwdText')}
        cancelTitle={t('alert.action.cancel')}
        onCancel={handleCancelPwd}
        confirmTitle={t('alert.action.ok')}
        onConfirm={handleSavePwd}
      >
        <div className="profileModalEditPwd">
          <TextInput 
            leftIcon="lock"
            leftIconColor="var(--color-primary)"
            placeholder={t('input.currentPwd')}
            className={errors.pwd ? 'inputs error' : 'inputs'}
            type={showPwd ? "text" : "password"}
            rightIcon={showPwd ? "eye-slash" : "eye"} rightIconType="solid"
            rightIconSize="1x" rightIconColor="var(--color-black-light)"
            onRightIconClick={handleShowPwd}
            value={pwd}
            onChange={(e) => setPwd(e.target.value)}
          />
          <TextInput 
            leftIcon="lock"
            leftIconColor="var(--color-primary)"
            placeholder={t('input.newPwd')}
            className={errors.pwdNew ? 'inputs error' : 'inputs'}
            type={showPwdNew ? "text" : "password"}
            rightIcon={showPwdNew ? "eye-slash" : "eye"} rightIconType="solid"
            rightIconSize="1x" rightIconColor="var(--color-black-light)"
            onRightIconClick={handleShowPwdNew}
            value={pwdNew}
            onChange={(e) => setPwdNew(e.target.value)}
          />
          <TextInput 
            leftIcon="lock"
            leftIconColor="var(--color-primary)"
            placeholder={t('input.confirmPwd')}
            className={errors.pwdConfirm ? 'inputs error' : 'inputs'}
            type={showPwdConfirm ? "text" : "password"}
            rightIcon={showPwdConfirm ? "eye-slash" : "eye"} rightIconType="solid"
            rightIconSize="1x" rightIconColor="var(--color-black-light)"
            onRightIconClick={handleShowPwdConfirm}
            value={pwdConfirm}
            onChange={(e) => setPwdConfirm(e.target.value)}
          />
          {errors && errors.pwdStatus &&<div className="pwdError"> {t(errors.pwdStatus)}</div>}
        </div>
      </AlertModal>
    </PageLayout>
  );
};

export {Profile};