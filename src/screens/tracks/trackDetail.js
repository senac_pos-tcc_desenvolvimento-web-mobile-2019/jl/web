import React from 'react';
import { useTranslation } from "react-i18next";
import { useParams, Redirect, useHistory } from 'react-router-dom';

import { usePublishedTracks, useTrackCoursesInfo } from '@growth/commons/context';

import defaultLogo from '../../assets/LogoScout.png';
import { PageLayout, PageHeader, TrackProgressCard, TrackInfoBar, CourseCard } from '../../components';
import './detailStyle.css';


const Track = () => {
  const { id } = useParams();
  const track = usePublishedTracks(tracks => tracks.find(track => track.id == id));

  if (!track)
    return (<Redirect to="/tracks" />);
  else
    return (<TrackPage track={track} />);
};

const TrackPage = ({ track }) => {
  const {t} = useTranslation();
  const history = useHistory();
  const coursesInfo = useTrackCoursesInfo(track);

  return (
    <PageLayout
      title={t('menu.tracks')}
      renderHeader={() => (
        <React.Fragment>
          <PageHeader badge={defaultLogo} title={track.name} description={track.description}>
            <TrackProgressCard track={track} />
          </PageHeader>
          <TrackInfoBar track={track} />
        </React.Fragment>
      )}
    >
      <div className="trackDetailDescription">
        {track.text}
      </div>

      {!!coursesInfo.courses && coursesInfo.courses.map(course => (
        <CourseCard
          key={course.id}
          course={course}
          onClick={() => history.push('/courses/' + course.id)}
        />
      ))}
    </PageLayout>
  );
};

export {Track};