import React, { useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import { Strings } from '@growth/commons/utils';
import { usePublishedTracks, useUserTracks } from '@growth/commons/context';

import { PageLayout, TrackCard, Pagination, SearchBar } from '../../components';
import './style.css';

const Tracks = () => {
  const {t} = useTranslation();
  const allTracks = usePublishedTracks();
  const userTracks = useUserTracks();
  const history = useHistory();
  const location = useLocation();

  const [selectedButton, setSelectedButton] = useState(location.state || 'all');
  const [search, setSearch] = useState('');

  const availableList = selectedButton == 'all' ? allTracks : userTracks;
  const seachedList = !search.trim().length ? availableList : availableList.filter(track => {
    return Strings.containsSearch(track.name, search);
  });

  useEffect(() => {
    setSelectedButton(location.state || 'all');
  }, [location.state]);

  const renderTracksContainer = (courses) => {
    return (<div className="tracksListCards">{courses}</div>)
  };

  const renderTrack = (track) => (<TrackCard key={track.id} track={track} onClick={() => history.push('/tracks/' + track.id)} />);

  return (
    <PageLayout title={t('menu.tracks')}>
      <SearchBar
        legend={t('info.showMe') + ':'}
        buttons={[
          { title: t('button.allTracks'), onClick: () => setSelectedButton('all'), selected: selectedButton == 'all' },
          { title: t('button.myTracks'), onClick: () => setSelectedButton('my'), selected: selectedButton == 'my' }
        ]}
        search={search}
        onSearchChange={(e) => setSearch(e.target.value)}
      />
      <Pagination
        records={seachedList}
        recordsPerPage={4}
        renderRecordsContainer={renderTracksContainer}
        renderRecord={renderTrack}
        emptyMessage="Nenhuma trilha encontrada"
      />
    </PageLayout>
  );
}

export {Tracks};

