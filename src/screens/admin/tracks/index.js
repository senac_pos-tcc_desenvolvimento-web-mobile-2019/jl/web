import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import { Strings } from '@growth/commons/utils';
import { useTracks, useUsers } from '@growth/commons/context';

import { PageLayout, Icon, Pagination, SearchBar, AlertModal } from '../../../components';
import './style.css';


const AdminTracks = () => {
  const {t} = useTranslation();
  const [search, setSearch] = useState('');
  const [selectedTrack, setSelectedTrack] = useState();
  const history = useHistory();

  const users = useUsers();
  const tracks = useTracks();

  const seachedTracks = !search.trim().length ? tracks : tracks.filter(track => {
    return Strings.containsSearch(track.name, search);
  });

  const handleAdd = () => {
    history.push('/adminTracks/manage');
  };

  const handleUpdateTrack = (id) => {
    history.push('/adminTracks/manage/' + id);
  };

  const handleDeleteClick = (course) => {
    setSelectedTrack(course);
  };

  const handleDeleteCancel = () => {
    setSelectedTrack();
  };

  const handleDeleteConfirm = () => {
    selectedTrack.delete();
    setSelectedTrack();
  };

  const renderTracks = (track, index) => {
    let zebra = index%2;
    const author = users.find((user)=>user.id === track.createdBy);
    const maintainer = users.find((user)=>user.id === track.updatedBy) || author;
    const targets = track.targets.map(target => t(`roles.${target}`)).join(', ');

    return(
      <tr key={track.id} className={ zebra === 0 ? "color" : "white" }>
        <td>{track.name}</td>
        <td>{targets}</td>
        <td>{track.courses.length}</td>
        <td>{(track.updatedAt || track.createdAt).toLocaleString()}</td>
        <td>{maintainer && maintainer.name}</td>
        <td>{t(`status.${track.status}`)}</td>
        <td className="admTracksActions">
          <Icon name="edit" type="regular" size="1x" color="var(--color-black)" className="admCoursesIcon" onClick={()=>{handleUpdateTrack(track.id)}}/>
          <Icon name="trash-alt" type="regular" size="1x" color="var(--color-black)" className="admCoursesIcon" onClick={()=>{handleDeleteClick(track)}} />
        </td>
      </tr> 
    );
  };

  const renderTracksTable = (records) => {
    return (
      <table>
        <thead>
          <tr>
            <th scope="col">{t('adminTracks.info.title')}</th>
            <th scope="col">{t('adminTracks.info.roles')}</th>
            <th scope="col">{t('adminTracks.info.courses')}</th>
            <th scope="col">{t('adminTracks.info.updatedAt')}</th>
            <th scope="col">{t('adminTracks.info.updatedBy')}</th>
            <th scope="col">{t('adminTracks.info.status')}</th>
            <th scope="col">{t('adminTracks.info.actions')}</th>
          </tr>
        </thead>
        <tbody>
          {records}
        </tbody>
      </table>
    );
  };

  return(
    <PageLayout title={t('menu.manageTracks')}>
      <SearchBar
        buttons={[
          { title: t('button.addTrack'), onClick:() => handleAdd(), selected: true },
        ]}
        search={search}
        onSearchChange={(e) => setSearch(e.target.value)}
      />

      <div className="admTracksBody">
        <Pagination records={seachedTracks} recordsPerPage={10} renderRecordsContainer={renderTracksTable} renderRecord={renderTracks} />
      </div>

      <AlertModal
        visible={!!selectedTrack}
        title={t('alert.title.atention')}
        text={t('alert.text.deleteTrack')}
        cancelTitle={t('alert.action.cancel')}
        onCancel={handleDeleteCancel}
        confirmTitle={t('alert.action.confirm')}
        onConfirm={handleDeleteConfirm}
      />
    </PageLayout>
  );
};

export {AdminTracks};