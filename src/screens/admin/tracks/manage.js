import React, { Fragment, useRef, useState } from 'react';
import { useTranslation } from "react-i18next";
import { useHistory, useParams } from 'react-router-dom';
import Draggable, { DraggableCore } from 'react-draggable'

import { Constants } from '@growth/commons/utils';
import { Track } from '@growth/commons/models';
import { useTracks, useCourses } from '@growth/commons/context';

import { PageLayout, PageFooter, InputText, TextAreaInput, InputRadioGroup, 
        InputCheckboxGroup, InputCheckBox, Button, Dropzone, Badge, TechnicalCard, AlertModal, Icon } from '../../../components';
import './manageStyle.css';


const validateTrackData = (data, courses) => {
  const errors = {};

  if (data.status == Constants.PUBLISH_STATUS.DRAFT)
    return errors;
  if (!(data.name && data.name.length))
    errors.name = true;
  if (!(data.description && data.description.length))
    errors.description = true;
  if (!(data.targets && data.targets.length))
    errors.targets = true;
  if (!data.courses.length) {
    errors.emptyCourses = true;
  } else {
    const invalidCoursesIds = [];
    data.courses.forEach(({ id }) => {
      const course = courses.find(c => c.id == id);
      const targetMismatch = !!course && (
        !data.targets.length || !course.targets || !course.targets.length || 
        data.targets.some(target => !course.targets.includes(target)) ||
        course.targets.some(target => !data.targets.includes(target))
      );
      if (!course || targetMismatch)
        invalidCoursesIds.push(id);
    });

    if (invalidCoursesIds.length)
      errors.courses = invalidCoursesIds;
  }

  errors.count = Object.keys(errors).length;
  return errors;
};

const sortByPosition = (course1, course2) => {
  return course1.index - course2.index;
};

const restorePosition = (courses) => {
  courses.forEach((course, index) => course.index = index);
};

const ManageTrack = () => {
  const pageLayout = useRef();
  const dropzone = useRef();

  const {t} = useTranslation();
  const history = useHistory();
  const { id } = useParams();

  const track = useTracks(tracks => tracks.find(track => track.id == id));
  const allCourses = useCourses();

  const [loading, setLoading] = useState(false);
  const [errorModalVisible, setErrorModalVisible] = useState(false);
  const [coursesModalVisible, setCoursesModalVisible] = useState(false);
  const [errors, setErrors] = useState({ });
  const [status, setStatus] = useState((track && track.status) || Constants.PUBLISH_STATUS.DRAFT);
  const [name, setName] = useState((track && track.name) || '');
  const [description, setDescription] = useState((track && track.description || ''));
  const [targets, setTargets] = useState((track && [...track.targets]) || []);
  const [text, setText] = useState((track && track.text) || '');
  const [courses, setCourses] = useState((track && [...track.courses]) || []);
  const [badgeUrl, setBadgeUrl] = useState((track && track.badgeUrl) || '');
  const [selectedCourses, setSelectedCourses] = useState([]);
  const [draggingData, setDraggingData] = useState(null)

  const availableCourses = allCourses.filter(course => (
    course.status == Constants.PUBLISH_STATUS.PUBLISHED &&
    !courses.some(({ id }) => id == course.id) &&
    targets.every(target => course.targets.includes(target)) &&
    course.targets.every(target => targets.includes(target))
  ));

  const roleOptions = [
    { value: Constants.ROLE.EMPLOYEE, title: t(`roles.${Constants.ROLE.EMPLOYEE}`) },
    { value: Constants.ROLE.CLIENT, title: t(`roles.${Constants.ROLE.CLIENT}`) }
  ];

  const readOnly = status != Constants.PUBLISH_STATUS.DRAFT;
  const limitDescription = 150;

  const handleBadgeError = (error) => {
    console.log('SALTA UM ERRO PRA ', error);
  };

  const handleBadgeUpload = async (track) => {
    if (dropzone.current) {
      const upBadgeUrl = await dropzone.current.upload('tracks/badge/' + track.id);
      if (track.badgeUrl != upBadgeUrl)
        return await track.update({ badgeUrl: upBadgeUrl ? upBadgeUrl : null });
      else
        return;
    }
  };

  const handleSave = (status) => {
    setLoading(true);

    const data = { status, name, description, targets, text, courses };
    let errors = { };

    if (!(track && track.status == Constants.PUBLISH_STATUS.PUBLISHED && status == Constants.PUBLISH_STATUS.READY))
      errors = validateTrackData(data, allCourses);

    if (errors.count) {
      setErrors(errors);
      setErrorModalVisible(true);
      setLoading(false);
    } else {
      setErrors({ }); 
      if (track)
        track.update(data).then(() => {
          handleBadgeUpload(track).then(() => {
            setStatus(status);
            setLoading(false);
            setBadgeUrl(track.badgeUrl);
          });
        });
      else
        Track.create(data).then((trackDoc) => {
          const createdTrack = Track.fromDoc(trackDoc);
          handleBadgeUpload(createdTrack).then(() => {
            history.replace(history.location.pathname + '/' + createdTrack.id);
            setLoading(false);
          });
        });
    }

    pageLayout.current.scrollToTop();
  };

  const handleDescription = (text) => {
    text.length > limitDescription ? setDescription(text.slice(0, limitDescription)) : setDescription(text);
  };

  const handleCourseDelete = (courseId) => {
    const remainingCourses = courses.filter(({ id }) => id != courseId);
    restorePosition(remainingCourses);
    setCourses(remainingCourses);
  };

  const closeCoursesModal = () => {
    setSelectedCourses([]);
    setCoursesModalVisible(false)
  };

  const handleCourseAdd = () => {
    const newCourses = [ ...courses ];
    selectedCourses.forEach(id => newCourses.push({ id }));
    restorePosition(newCourses);
    setCourses(newCourses);
    closeCoursesModal();
  };

  const handleCourseDrag = (event, dragData, course) => {
    const indexDelta = Math.ceil((Math.abs(dragData.y) - (CARD_HEIGHT / 2)) / CARD_HEIGHT);
    const newIndex = (dragData.y > 0) ? (course.index + indexDelta) : (course.index - indexDelta);
    if (!draggingData || draggingData.newIndex != newIndex)
      setDraggingData({ course, newIndex })

    return true
  }

  const handleCourseDragStop = (event, dragData, course) => {
    if (draggingData && draggingData.newIndex != course.index) {
      const newCourses = courses.filter(({ index }) => index != course.index);
      newCourses.splice(draggingData.newIndex, 0, course);
      restorePosition(newCourses);
      setDraggingData(null);
      setCourses(newCourses);
    }
  }

  return(
    <PageLayout
      ref={pageLayout}
      loading={loading}
      title={t('menu.manageTracks')}
      renderFooter={() => (
        <PageFooter>
          {status == Constants.PUBLISH_STATUS.DRAFT  && (
            <Fragment>
              <Button title={t("button.draft")} onClick={() => handleSave(Constants.PUBLISH_STATUS.DRAFT) } className="manageTrackMargin"/>
              <Button title={t("button.finish")} onClick={() => handleSave(Constants.PUBLISH_STATUS.READY)} className="manageTrackMargin" />
            </Fragment>
          )}
          {status == Constants.PUBLISH_STATUS.READY && (
            <Fragment>
              <Button title={t("button.edit")} onClick={() => setStatus(Constants.PUBLISH_STATUS.DRAFT)} className="manageTrackMargin"/>
              <Button title={t("button.publish")} onClick={() => handleSave(Constants.PUBLISH_STATUS.PUBLISHED)} className="manageTrackMargin"/>
            </Fragment>
          )}
          {status == Constants.PUBLISH_STATUS.PUBLISHED && (
            <Button title={t("button.pause")} onClick={() => handleSave(Constants.PUBLISH_STATUS.READY)} className="manageTrackMargin"/>
          )}
        </PageFooter>
      )}
    >

      <div className="manageTrackTop">
        <div className="manageTrackInfo">
          <InputText
            className={errors.name ? 'error' : ''}
            title={t("adminTracks.info.title")}
            required={true}
            readonly={readOnly}
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <div className="manageTrackShortDescription">
            <TextAreaInput
              className={errors.description ? 'error' : ''}
              title={t("adminTracks.info.shortDescription")}
              required={true}
              numRows={2}
              readonly={readOnly}
              value={description}
              onChange={(e) => handleDescription(e.target.value)}
            />
            {!readOnly && <span className="manageTrackShortDescription_span">{description.length}/{limitDescription}</span>}
          </div>
          
          <InputCheckboxGroup
            className={`manageTrackOptionContainer ${errors.targets ? 'error' : ''}`}
            title={t("adminTracks.info.targetAudience")}
            required={true}
            readonly={readOnly}
            data={roleOptions}
            isSelected={roleOption => targets.includes(roleOption.value)}
            onChange={e => {
              if (e.target.checked)
                setTargets([ ...targets, e.target.value]);
              else
                setTargets([ ...targets.filter(r => r != e.target.value)]);
            }}
          />

          <TextAreaInput
            className={errors.text ? 'error' : ''}
            title={t("adminTracks.info.text")}
            numRows={5}
            readonly={readOnly}
            value={text}
            onChange={(e) => setText(e.target.value)}
          />
        </div>
        <div className="manageTrackBadge">
          {readOnly && <Badge url={badgeUrl} size={200} />}
          {!readOnly && (
            <Dropzone
              ref={dropzone}
              placeholder={t("adminTracks.info.badge")}
              type={Dropzone.IMAGE}
              previousFileUrl={badgeUrl}
              onValidationError={handleBadgeError}
              readonly={readOnly}
              className="dropzoneCourseBadge"
            />
          )}
        </div>
      </div>

      <div className="manageTrackBottomTitle">{t("title.trackCourses")}</div>

      <div className="manageTrackBottom">
        <div className="manageTrackBottomLeft">
          {courses.sort(sortByPosition).map(course => {
            const isDragging   = draggingData && draggingData.course.index == course.index;
            const shouldGoUp   = draggingData && draggingData.newIndex >= course.index && course.index > draggingData.course.index;
            const shouldGoDown = draggingData && draggingData.newIndex <= course.index && course.index < draggingData.course.index;

            const bounds = {
              top: (course.index * CARD_HEIGHT) * -1,
              bottom: (courses.length - 1 - course.index) * CARD_HEIGHT
            };

            const position = { x: 0, y: 0 };
            if (!isDragging) {
              if (shouldGoUp)
                position.y = CARD_HEIGHT * -1
              if (shouldGoDown)
                position.y = CARD_HEIGHT
            };

            return (
              <DraggableCourseCard
                courseId={course.id}
                index={course.index}
                targets={targets}
                readOnly={readOnly}
                error={!!errors.courses && errors.courses.includes(course.id)}
                onDelete={() => handleCourseDelete(course.id)}
                dragging={isDragging}
                axis='y'
                bounds={bounds}
                position={position}
                onStart={() => !readOnly}
                onDrag={(event, dragData) => handleCourseDrag(event, dragData, course)}
                onStop={(event, dragData) => handleCourseDragStop(event, dragData, course)}
              />
            );
          })}
        </div>

        <div className="manageTrackBottomRight">
          {!readOnly && (
            <Button title="Adicionar treinamento" className="square" onClick={() => setCoursesModalVisible(true)} />
          )}
          {readOnly && (
            <TechnicalCard
              createdAt={track.createdAt}
              createdBy={track.createdBy}
              updatedAt={track.updatedAt || track.createdAt}
              courses={courses.length}
              status={track.status}
            />
          )}
        </div>
      </div>

      <AlertModal
        visible={errorModalVisible}
        title="A trilha ainda não está pronta!"
        confirmTitle={t('alert.action.ok')}
        onConfirm={() => setErrorModalVisible(false)}
      >
        <p>
          Certifique-se de que todos os campos obrigatórios estejam devidamente preenchidos. Também é obrigatório que todos os treinamentos adicionados sejam destinados exatamente ao mesmo público definido na trilha.
        </p>
      </AlertModal>

      <AlertModal
        visible={coursesModalVisible}
        title="Adicionar treinamentos à trilha"
        text="Selecione os treinamentos que serão adicionados à trilha"
        confirmTitle={t('alert.action.confirm')}
        confirmDisabled={!selectedCourses.length}
        onConfirm={handleCourseAdd}
        cancelTitle={t('alert.action.cancel')}
        onCancel={closeCoursesModal}
        contentClassName="manageTrackCoursesModal"
      >
        <table>
          <thead>
            <tr>
              <th scope="col"></th>
              <th scope="col">{t('adminCourses.info.title')}</th>
              <th scope="col">{t('adminCourses.info.shortDescription')}</th>
              <th scope="col">{t('adminCourses.info.updatedAt')}</th>
            </tr>
          </thead>
          <tbody>
            {availableCourses.map((course, index) => {
              const zebra = index%2;
              const isSelected = selectedCourses.includes(course.id);
              return (
                <tr key={course.id} className={ zebra ? "color" : "white" }>
                  <td>
                    <InputCheckBox
                      key={course.id}
                      id={course.id}
                      checked={isSelected}
                      onChange={() => {
                        if (isSelected)
                          setSelectedCourses(selectedCourses.filter(id => id != course.id));
                        else
                          setSelectedCourses([ ...selectedCourses, course.id ]);
                      }}
                    />
                  </td>
                  <td>{course.name}</td>
                  <td>{course.description}</td>
                  <td>{(course.updatedAt || course.createdAt).toLocaleString()}</td>
                </tr>
              );
            })}

            {!availableCourses.length && (
              <tr className="empty">
                <td colspan="4">
                  Nenhum treinamento encontrado para o público alvo da trilha que já não tenha sido adicionado.
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </AlertModal>
    </PageLayout>
  );
};

const CARD_HEIGHT = 144;

const DraggableCourseCard = ({ courseId, index, targets = [], className = '', readOnly, error, onDelete, ...otherProps }) => {
  const {t} = useTranslation();
  const course = useCourses(courses => courses.find(course => course.id == courseId)) || {
    id: courseId,
    name: 'Treinamento inexistente',
    description: 'Este treinamento já não existe mais e é seguro removê-lo da trilha.',
    targets: []
  };

  if (course) {
    return (
      <Draggable { ...otherProps }>
        <div key={course.id} className={`draggableCourseCard ${readOnly ? 'readOnly' : ''} ${className}`}>
          <span className="courseCardPosition">{index + 1}</span>
          <div className={`courseCardContentContainer ${error ? 'error' : course.status != Constants.PUBLISH_STATUS.PUBLISHED ? 'disabled' : ''}`}>
            <div className="courseCardLogo">
              <Badge url={course.badgeUrl} />
            </div>
            <div className="courseCardtexts">
              <div className="courseCardTitle">{course.name}</div>
              <div>{course.description}</div>
              <div className="courseCardStatus">
                <small>
                  {course.targets.map(target => t(`roles.${target}`)).join(', ')} - {t(`status.${course.status}`)}
                </small>
              </div>
            </div>
          </div>
          {!readOnly && (
            <div className="courseCardDelete" onClick={onDelete}>
              <Icon name="trash-alt" size="1x" />
            </div>
          )}
        </div>
      </Draggable>
    );
  }

  return null;
};

export {ManageTrack};