import React, { useState } from 'react';
import { useTranslation } from "react-i18next";

import { PageLayout, PageHeader, PageFooter, Button, Icon, AlertModal, Dropzone } from '../../../components';
import badge from '../../../assets/badgeGreen.png'; 

import './style.css'

const Framework = () => {
  const {t} = useTranslation();

  const [modalVisible, setModalVisible] = useState(false);

  const handleButtonClick = () => setModalVisible(true);

  return(
    <PageLayout
      title={t('menu.frame')}
      subtitle="Coleção de componentes disponíveis pra uso"
      renderStickyHeader={() => (
        <PageHeader badge={badge} title="Título do header da página" description="descrição do header da página">
          conteúdo do header
        </PageHeader>
      )}
      renderStickyFooter={() => (
        <PageFooter>
          Conteúdo do rodapé da página
        </PageFooter>
      )}
    >
      <div className="framework">
        <Icon name="facebook" type="brands" size="2x" color="var(--color-black)" />

        <div>
          <Button title="Normal" onClick={handleButtonClick} />
          <Button title="Normal disabled" disabled={true} onClick={handleButtonClick} />
        </div>
        <div>
          <Button title="Normal reverse" className="reverse" onClick={handleButtonClick} />
          <Button title="Normal reverse disabled" className="reverse" disabled={true} onClick={handleButtonClick} />
        </div>
        <div>
          <Button title="Quadrado" className="square" onClick={handleButtonClick} />
          <Button title="Quadrado disabled" className="square" disabled={true} onClick={handleButtonClick} />
        </div>
        <div>
          <Button title="Quadrado reverse" className="square reverse" onClick={handleButtonClick} />
          <Button title="Quadrado reverse disabled" className="square reverse" disabled={true} onClick={handleButtonClick} />
        </div>
      </div>

      <Dropzone placeholder="Upload de arquivos" type={Dropzone.IMAGE} maxSize={52000} />
      <Dropzone placeholder="Upload de arquivos" type={Dropzone.IMAGE} maxSize={52000} className="dropzoneSquare"/>

      <AlertModal
        visible={modalVisible}
        title="Botão pressionado!"
        text="O botão foi pressionado e lançou esta mensagem..."
        confirmTitle="OK"
        onConfirm={() => setModalVisible(false)}
      />
    </PageLayout>
  );
}

export {Framework};