import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';
import { useTranslation } from "react-i18next";

import { Strings } from '@growth/commons/utils';
import { useCourses, useUsers } from '@growth/commons/context';

import { PageLayout, Icon, Pagination, SearchBar, AlertModal } from '../../../components';
import './style.css';


const AdminCourses = () => {
  const {t} = useTranslation();
  const [search, setSearch] = useState('');
  const [selectedCourse, setSelectedCourse] = useState();
  const history = useHistory();

  const courses = useCourses();
  const users = useUsers();

  const searchedCourses = !search.trim().length ? courses : courses.filter(course => {
    return Strings.containsSearch(course.name, search);
  });

  const handleSearch = (e) => {
    setSearch(e.target.value);
  }

  const handleAdd = () => {
    history.push('/adminCourses/manage');
  };

  const handleUpdateCourse = (id) => {
    history.push('/adminCourses/manage/' + id);
  };

  const handleDeleteClick = (course) => {
    setSelectedCourse(course);
  };

  const handleDeleteCancel = () => {
    setSelectedCourse();
  };

  const handleDeleteConfirm = () => {
    selectedCourse.delete();
    setSelectedCourse();
  };

  const renderCourses = (course, index) => {
    let zebra = index%2;
    const author = users.find((user)=>user.id === course.createdBy);
    const maintainer = users.find((user)=>user.id === course.updatedBy) || author;
    const targets = course.targets.map(target => t(`roles.${target}`)).join(', ');

    return(
      <tr key={course.id} className={ zebra === 0 ? "color" : "white" }>
        <td>{course.name}</td>
        <td>{targets}</td>
        <td>{author && author.name}</td>
        <td>{course.date.toLocaleString()}</td>
        <td>{maintainer && maintainer.name}</td>
        <td>{t(`status.${course.status}`)}</td>
        <td className="admCoursesActions">
          <Icon name="edit" type="regular" size="1x" color="var(--color-black)" className="admCoursesIcon" onClick={()=>{handleUpdateCourse(course.id)}}/>
          <Icon name="trash-alt" type="regular" size="1x" color="var(--color-black)" className="admCoursesIcon" onClick={()=>{handleDeleteClick(course)}} />
        </td>
      </tr> 
    );
  };

  const renderCoursesTable = (records) => {
    return (
      <table>
        <thead>
          <tr>
            <th scope="col">{t('adminCourses.info.title')}</th>
            <th scope="col">{t('adminCourses.info.roles')}</th>
            <th scope="col">{t('adminCourses.info.author')}</th>
            <th scope="col">{t('adminCourses.info.updatedAt')}</th>
            <th scope="col">{t('adminCourses.info.updatedBy')}</th>
            <th scope="col">{t('adminCourses.info.status')}</th>
            <th scope="col">{t('adminCourses.info.actions')}</th>
          </tr>
        </thead>
        <tbody>
          {records}
        </tbody>
      </table>
    );
  };

  return(
    <PageLayout title={t('menu.manageCourses')}>
      <SearchBar
        buttons={[ { title: t('button.addCourse'), onClick: handleAdd, selected: true } ]}
        search={search}
        onSearchChange={(e) => setSearch(e.target.value)}
      />

      <div className="admCoursesBody">
        <Pagination records={searchedCourses} recordsPerPage={10} renderRecordsContainer={renderCoursesTable} renderRecord={renderCourses} />
      </div>

      <AlertModal
        visible={!!selectedCourse}
        title={t('alert.title.atention')}
        text={t('alert.text.deleteCourse')}
        cancelTitle={t('alert.action.cancel')}
        onCancel={handleDeleteCancel}
        confirmTitle={t('alert.action.confirm')}
        onConfirm={handleDeleteConfirm}
      />
    </PageLayout>
  );
};

export {AdminCourses};