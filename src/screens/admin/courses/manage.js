import React, { Fragment, useRef, useState } from 'react';
import { useTranslation } from "react-i18next";
import { useHistory, useParams } from 'react-router-dom';

import { Constants } from '@growth/commons/utils';
import { Course } from '@growth/commons/models';
import { useCourses } from '@growth/commons/context';

import { PageLayout, PageFooter, InputText, TextAreaInput, InputRadioGroup, 
        InputCheckboxGroup, Button, Dropzone, Badge, TechnicalCard, AlertModal } from '../../../components';
import './manageStyle.css';


const validateCourseData = (data) => {
  const errors = {};

  if (data.status == Constants.PUBLISH_STATUS.DRAFT)
    return errors;
  if (!(data.name && data.name.length))
    errors.name = true;
  if (!(data.description && data.description.length))
    errors.description = true;
  if (!(data.targets && data.targets.length))
    errors.targets = true;
  if (isNaN(parseInt(data.estimatedTime)))
    errors.estimatedTime = true;

  const noVideo = data.mediaType == 'video' && !(data.videoUrl && data.videoUrl.length);
  const noImage = data.mediaType == 'image' && !(data.imageUrl && data.imageUrl.length);
  const noText  = !(data.text && data.text.length);

  if (noText && noVideo) {
    errors.videoUrl = true;
    errors.text = true;
  }
  if (noText &&  noImage) {
    errors.imageUrl = true;
    errors.text = true;
  }

  errors.count = Object.keys(errors).length;
  return errors;
};

const ManageCourse = () => {
  const pageLayout = useRef();
  const dropzone = useRef();
  const dropzoneImage = useRef();

  const {t} = useTranslation();
  const history = useHistory();
  const { id } = useParams();

  const roles = [Constants.ROLE.EMPLOYEE, Constants.ROLE.CLIENT];
  const course = useCourses(courses => courses.find(course => course.id == id));

  const [loading, setLoading] = useState(false);
  const [errorModalVisible, setErrorModalVisible] = useState(false);
  const [errors, setErrors] = useState({ });
  const [status, setStatus] = useState((course && course.status) || Constants.PUBLISH_STATUS.DRAFT);
  const [name, setName] = useState((course && course.name) || '');
  const [description, setDescription] = useState((course && course.description || ''));
  const [targets, setTargets] = useState((course && [...course.targets]) || []);
  const [estimatedTime, setEstimatedTime] = useState((course && course.estimatedTime) || '');
  const [mediaType, setMediaType] = useState((course && course.mediaType) || 'video');
  const [videoUrl, setVideoUrl] = useState((course && course.videoUrl) || '');
  const [imageUrl, setImageUrl] = useState((course && course.imageUrl) || '');
  const [badgeUrl, setBadgeUrl] = useState((course && course.badgeUrl) || '');
  const [text, setText] = useState((course && course.text) || '');
  
  const readOnly = status != Constants.PUBLISH_STATUS.DRAFT;
  const limitDescription = 150;

  const roleOptions = [
    { value: Constants.ROLE.EMPLOYEE, title: t(`roles.${Constants.ROLE.EMPLOYEE}`) },
    { value: Constants.ROLE.CLIENT, title: t(`roles.${Constants.ROLE.CLIENT}`) }
  ];

  const mediaTypeOptions = [
    { value: 'video', title: t("info.video"), rightIcon: 'youtube', rightIconType: 'brands' },
    { value: 'image', title: t("info.image"), rightIcon: 'image', rightIconType: 'regular' }
  ];

  const handleBadgeError = (error) => {
    console.log('SALTA UM ERRO PRA ', error);
  };

  const handleBadgeUpload = async (course) => {
    if (dropzone.current) {
      const upBadgeUrl = await dropzone.current.upload('courses/badge/' + course.id);
      if (course.badgeUrl != upBadgeUrl)
        return await course.update({ badgeUrl: upBadgeUrl ? upBadgeUrl : null });
      else
        return;
    }
  };

  const handleImageUpload = async (course) => {
    if (dropzoneImage.current) { 
      const upImageUrl = await dropzoneImage.current.upload('courses/content/' + course.id);
      if (course.imageUrl != upImageUrl) 
        return await course.update({ imageUrl: upImageUrl ? upImageUrl : null });
      else
        return;
    }
  };

  const handleSave = (status) => {
    setLoading(true);
    const data = { status, name, description, targets, estimatedTime, mediaType, videoUrl, imageUrl, text };
    const errors = validateCourseData(data);

    if (errors.count) {
      setErrors(errors);
      setErrorModalVisible(true);
      setLoading(false);
    } else {
      setErrors({ }); 
      if (course)
        course.update(data).then(() => { 
          handleBadgeUpload(course).then(() => {
            handleImageUpload(course).then (() => {
              setStatus(status);
              setLoading(false);
              setBadgeUrl(course.badgeUrl);
              setImageUrl(course.imageUrl);
            })
          });
        });
      else
        Course.create(data).then((courseDoc) => {
          const createdCourse = Course.fromDoc(courseDoc);
          handleBadgeUpload(createdCourse).then(() => {
            history.replace(history.location.pathname + '/' + createdCourse.id);
            setLoading(false);
          });
        });
    }

    pageLayout.current.scrollToTop();
  };

  const handleDescription = (text) => {
    text.length > limitDescription ? setDescription(text.slice(0, limitDescription)) : setDescription(text);
  };
console.log("badgeURL", badgeUrl);

  return(
    <PageLayout
      ref={pageLayout}
      loading={loading}
      title={t('menu.manageCourses')}
      renderFooter={() => (
        <PageFooter>
          {status == Constants.PUBLISH_STATUS.DRAFT  && (
            <Fragment>
              <Button title={t("button.draft")} onClick={() => handleSave(Constants.PUBLISH_STATUS.DRAFT) } className="manageCourseMargin"/>
              <Button title={t("button.finish")} onClick={() => handleSave(Constants.PUBLISH_STATUS.READY)} className="manageCourseMargin" />
            </Fragment>
          )}
          {status == Constants.PUBLISH_STATUS.READY && (
            <Fragment>
              <Button title={t("button.edit")} onClick={() => setStatus(Constants.PUBLISH_STATUS.DRAFT)} className="manageCourseMargin"/>
              <Button title={t("button.publish")} onClick={() => handleSave(Constants.PUBLISH_STATUS.PUBLISHED)} className="manageCourseMargin"/>
            </Fragment>
          )}
          {status == Constants.PUBLISH_STATUS.PUBLISHED && (
            <Button title={t("button.pause")} onClick={() => handleSave(Constants.PUBLISH_STATUS.READY)} className="manageCourseMargin"/>
          )}
        </PageFooter>
      )}
    >

      <div className="manageCourseTop">
        <div className="manageCourseInfo">
          <InputText
            className={errors.name ? 'error' : ''}
            title={t("adminCourses.info.title")}
            required={true}
            readonly={readOnly}
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <div className="manageCourseShortDescription">
            <TextAreaInput
              className={errors.description ? 'error' : ''}
              title={t("adminCourses.info.shortDescription")}
              required={true}
              numRows={2}
              readonly={readOnly}
              value={description}
              onChange={(e) => handleDescription(e.target.value)}
            />
            {!readOnly && <span className="manageCourseShortDescription_span">{description.length}/{limitDescription}</span>}
          </div>
          
          <InputCheckboxGroup
            className={`manageCourseOptionContainer ${errors.targets ? 'error' : ''}`}
            title={t("adminCourses.info.targetAudience")}
            required={true}
            readonly={readOnly}
            data={roleOptions}
            isSelected={roleOption => targets.includes(roleOption.value)}
            onChange={e => {
              if (e.target.checked)
                setTargets([ ...targets, e.target.value]);
              else
                setTargets([ ...targets.filter(r => r != e.target.value)]);
            }}
          />

          <InputText
            className={errors.estimatedTime ? 'error' : ''}
            title={t("adminCourses.info.estimatedTime")}
            required={true}
            readonly={readOnly}
            value={estimatedTime} 
            type="number"
            onChange={(e) => setEstimatedTime(parseInt(e.target.value))}
            min="0"
            legend={t("legend.estimatedTime")}
            short = "short"
            rightLegend={t("info.minutes")}
          />
        </div>
        <div className="manageCourseBadge">
          {readOnly && <Badge url={badgeUrl} size={200} />}
          {!readOnly && (
            <Dropzone
              ref={dropzone}
              placeholder={t("adminCourses.info.badge")}
              type={Dropzone.IMAGE}
              previousFileUrl={badgeUrl}
              onValidationError={handleBadgeError}
              readonly={readOnly}
              className="dropzoneCourseBadge"
            />
          )}
        </div>
      </div>

      <div className="manageCourseBottomTitle">{t("title.content")}</div>

      <div className="manageCourseBottom">
        <div className="manageCourseBottomLeft">
          {!readOnly && (
            <Fragment>
              <InputRadioGroup
                title="Mídia"
                data={mediaTypeOptions}
                onChange={(e)=> setMediaType(e.target.value)}
                isSelected={type => type.value == mediaType}
              />

              <div className="manageCourseMidiaUploadArea">
                {mediaType === 'video' && (
                  <InputText
                    className={errors.videoUrl ? 'error' : ''}
                    title="URL do vídeo"
                    value={videoUrl} 
                    legend={t("legend.urlVideo")}
                    placeholder="https://youtu.be/aioyGvBSC7w"
                    onChange={(e) => setVideoUrl(e.target.value)}
                  />
                )}
                {mediaType === 'image' && (
                  <Dropzone
                    ref={dropzoneImage}
                    placeholder={t("adminCourses.info.badge")}
                    type={Dropzone.IMAGE}
                    previousFileUrl={imageUrl}
                    onValidationError={handleBadgeError}
                    readonly={readOnly}
                    className="dropzoneCourseImage"
                    dropzoneSquare={true}
                  />
                )}
              </div>
            </Fragment>
          )}

          {readOnly && mediaType === 'video' && videoUrl.length != 0 && (
            <div className="manageCourseThumb">
              <iframe width="560" height="315" src={videoUrl} frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
            </div>
          )}
        
        
          {readOnly && mediaType === 'image' && imageUrl !=0 && (
            <div className="manageCourseThumb">
              <div className="manageCoursePreviewImage" style={{backgroundImage: `url(${imageUrl})`}}></div>     
            </div>
          )}

          <TextAreaInput
            className={errors.text ? 'error' : ''}
            title="Texto do treinamento"
            numRows={8}
            readonly={readOnly}
            value={text}
            onChange={(e) => setText(e.target.value)}
          />
        </div>

        <div className="manageCourseBottomRight">
          {readOnly && 
            <TechnicalCard 
              createdAt={course.createdAt}
              createdBy={course.createdBy}
              updatedAt={course.updatedAt || course.createdAt}
              estimatedTime={course.estimatedTime}
              status={course.status}/>}
        </div>
      </div>

      <AlertModal
        visible={errorModalVisible}
        title="O treinamento ainda não está pronto!"
        confirmTitle={t('alert.action.ok')}
        onConfirm={() => setErrorModalVisible(false)}
      >
        <p>
          Certifique-se de que todos os campos obrigatórios estejam devidamente preenchidos. Também é obrigatório que o treinamento tenha um conteúdo na forma de um texto ou de uma mídia (imagem ou vídeo).
        </p>
      </AlertModal>
    </PageLayout>
  );
};

export {ManageCourse};