import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import { Strings, Constants } from '@growth/commons/utils';
import { useUsers, useCurrentUser } from '@growth/commons/context';

import { PageLayout, Icon, Pagination, SearchBar, AlertModal, InputRadioButton } from '../../../components';
import './style.css';


const ROLES = Object.values(Constants.ROLE);

const AdminUsers = () => {
  const { t } = useTranslation();
  const user = useCurrentUser();
  const users = useUsers(users => users.filter(({ id }) => id != user.id));

  const [loading, setLoading] = useState(false);
  const [search, setSearch] = useState('');
  const [roleModalVisible, setRoleModalVisible] = useState(false);
  const [activeModalVisible, setActiveModalVisible] = useState(false);
  const [selectedUser, setSelectedUser] = useState();
  const [selectedRole, setSelectedRole] = useState();


  const searchedUsers = !search.trim().length ? users : users.filter(user => {
    return Strings.containsSearch(user.name, search) || Strings.containsSearch(user.email, search);
  });

  const handleRoleChangeClick = (user) => {
    setSelectedUser(user);
    setSelectedRole(user.role);
    setRoleModalVisible(true);
  };

  const handleRoleChangeCancel = () => {
    setRoleModalVisible(false);
    setSelectedUser(undefined);
    setSelectedRole(undefined);
  };

  const handleRoleChangeConfirm = () => {
    setLoading(true);
    setRoleModalVisible(false);
    selectedUser.update({ role: selectedRole }).then(() => {
      setSelectedUser(undefined);
      setSelectedRole(undefined);
      setLoading(false);
    });
  };

  const handleActiveChangeClick = (user) => {
    setSelectedUser(user);
    setActiveModalVisible(true);
  };

  const handleActiveChangeCancel = () => {
    setActiveModalVisible(false);
    setSelectedUser(undefined);
  };

  const handleActiveChangeConfirm = () => {
    setLoading(true);
    setActiveModalVisible(false);
    selectedUser.update({ active: !selectedUser.active }).then(() => {
      setSelectedUser(undefined);
      setLoading(false);
    });
  };

  const renderUser = (user, index) => {
    const zebra = index%2;

    return(
      <tr key={user.id} className={ `${zebra ? 'color' : 'white'} ${user.active ? '' : 'inactive'}` }>
        <td>{user.name}</td>
        <td>{user.email}</td>
        <td>{user.createdAt.toLocaleString()}</td>
        <td className="action">
          <span className="admUsersRole" onClick={() => handleRoleChangeClick(user)}>
            {t(`roles.${user.role}`)}
            <Icon name="pencil-alt" type="solid" size="1x" color="var(--color-black)" />
          </span>
        </td>
        <td className="admUsersActive action">
          <Icon
            name={`user-${user.active ? 'check' : 'slash'}`}
            type="solid"
            size="1x"
            color={`var(--color-${user.active ? 'primary' : 'black'})`}
            onClick={() => handleActiveChangeClick(user)}
          />
        </td>
      </tr>
    );
  };

  const renderUsersTable = (records) => {
    return (
      <table>
        <thead>
          <tr>
            <th scope="col">Nome</th>
            <th scope="col">E-mail</th>
            <th scope="col">Data de cadastro</th>
            <th scope="col">Perfil</th>
            <th scope="col">{t('adminTracks.info.actions')}</th>
          </tr>
        </thead>
        <tbody>
          {records}
        </tbody>
      </table>
    );
  };

  return(
    <PageLayout loading={loading} title={t('menu.manageUsers')}>
      <SearchBar
        search={search}
        onSearchChange={(e) => setSearch(e.target.value)}
      />

      <div className="admUsersBody">
        <Pagination records={searchedUsers} recordsPerPage={10} renderRecordsContainer={renderUsersTable} renderRecord={renderUser} />
      </div>

      <AlertModal
        visible={roleModalVisible}
        title={t('adminUsers.title.roleChange')}
        cancelTitle={t('alert.action.cancel')}
        onCancel={handleRoleChangeCancel}
        confirmTitle={t('alert.action.change')}
        onConfirm={handleRoleChangeConfirm}
      >
        <span className="modalContentUser">{selectedUser && selectedUser.name}</span>
        <div className="roleModalContentInputs">
          {ROLES.map(role => (
            <InputRadioButton
              key={role}
              id={role}
              value={role}
              onChange={(e)=> setSelectedRole(e.target.value)}
              checked={role == selectedRole}
              title={t(`roles.${role}`)}
            />
          ))}
        </div>
        <p className="modalContentTip">
          {t(`roles.explanation.${selectedRole}`)}
        </p>
      </AlertModal>

      <AlertModal
        visible={activeModalVisible}
        title={t(`adminUsers.title.activeChange.${selectedUser && !selectedUser.active}`)}
        cancelTitle={t('alert.action.cancel')}
        onCancel={handleActiveChangeCancel}
        confirmTitle={t('alert.action.confirm')}
        onConfirm={handleActiveChangeConfirm}
      >
        <span className="modalContentUser">{selectedUser && selectedUser.name}</span>
        <p className="modalContentTip">
          {selectedUser && t(`adminUsers.info.active.${!selectedUser.active}`)}
        </p>
      </AlertModal>
    </PageLayout>
  );
};

export {AdminUsers};