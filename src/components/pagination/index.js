import React, {useEffect, useState, useImperativeHandle} from 'react';
import {Icon} from '../index';
import './style.css';


const Pagination = React.forwardRef(({ records, recordsPerPage, renderRecordsContainer, renderRecord, emptyMessage }, ref) => {

  const totalPages = Math.ceil(records.length/recordsPerPage) || 1;
  const [currentPage, setCurrentPage] = useState(0);
  const [pageRecords, setPageRecords] = useState(getPageRecords({ records, recordsPerPage, currentPage }));

  const previousPage = () => {
    if (currentPage > 0)
      setCurrentPage(currentPage-1)
  };

  const nextPage = () => {
    if (currentPage < (totalPages - 1))
      setCurrentPage(currentPage+1)
  };

  useEffect(() => {
    const lastPage = totalPages - 1;
    if (currentPage > lastPage)
      setCurrentPage(lastPage);
  }, [records, totalPages]);

  useEffect(() => {
    setPageRecords(getPageRecords({ records, recordsPerPage, currentPage }));
  }, [records, recordsPerPage, currentPage]);

  useImperativeHandle(ref, () => ({
    setPage: (page) => {
      if (page < 1)
        setCurrentPage(0);
      else if (page > totalPages)
        setCurrentPage(totalPages - 1);
      else
        setCurrentPage(page - 1);
    }
  }));

  const renderPageRecords = () => {
    const recordsToRender = pageRecords.map((record, index) => {
      return renderRecord(record, index); 
    });

    if (renderRecordsContainer)
      return renderRecordsContainer(recordsToRender);
    else
      return (<div className="paginationContent">{recordsToRender}</div>);
  };

  if (records && records.length) {
    return(
      <div className="paginationContainer">
        {renderPageRecords()}
        <div className="paginationControl"> 
          <ul>
            <li onClick={previousPage}> 
              <Icon name="angle-double-left" type="solid" size="xs" color="var(--color-primary)" />
            </li>
            {[...Array(totalPages)].map((_, index) => (
              <li key={index} onClick={() => setCurrentPage(index)} className={index === currentPage ? 'active' : '' }>
                {index + 1}
              </li>
            ))}
            <li onClick={nextPage}>
              <Icon name="angle-double-right" type="solid" size="xs" color="var(--color-primary)" />
            </li>
          </ul>
        </div>
      </div>
    );
  } else {
    return(
      <div className="paginationContainer">
        {emptyMessage}
      </div>
    )
  }
  
});

const getPageRecords = ({ records, recordsPerPage, currentPage }) => {
  const start = recordsPerPage * currentPage;
  const end = start + recordsPerPage;
  const pageRecords = records.slice(start, end);

  return pageRecords;
};

export {Pagination};
