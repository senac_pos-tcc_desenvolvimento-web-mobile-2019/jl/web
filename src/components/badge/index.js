import React from 'react';

import DefautBadge from '../../assets/badgeGreen.png';
import './style.css';

const Badge = React.memo(({ className='', url, size = 60, ...otherProps }) => {
  const style = {
    height: `${size}px`,
    width: `${size}px`,
    backgroundImage: `url(${url || DefautBadge})`
  };

  return (
    <div className={`badge ${className}`} style={style} { ...otherProps } /> 
  );
});

export { Badge };