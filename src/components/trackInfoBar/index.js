import React from 'react';
import { useTranslation } from "react-i18next";

import { useTrackCoursesInfo, useUserTrackProgress } from '@growth/commons/context';

import { Icon } from '../index';
import './style.css';


const TrackInfoBar = ({ track, className = '' }) => {
  const {t} = useTranslation();
  const { courses, estimatedTime } = useTrackCoursesInfo(track);

  return(
    <div className={`trackInfoBarContainer ${className}`}>
      <TrackInfo
        iconName="clock"
        iconType="regular"
        label={`${estimatedTime} ${t("info.minutes")}`}
        value={t('info.totalEstimatedTime')}
      />

      <TrackInfo
        iconName="book-open"
        iconType="solid"
        label={courses.length}
        value={t('info.courses')}
      />

      <TrackInfo
        iconName="sync"
        iconType="solid"
        label={(track.updatedAt || track.createdAt).toLocaleString()}
        value={t('info.updatedAt')}
      />
    </div>
  );
};

const TrackInfo = React.memo(({ iconName, iconType, label, value, className = '' }) => (
  <div className="trackInfoContainer">
    <Icon name={iconName} size="2x" color="var(--color-gray)" type={iconType} />
    <div className="trackInfoText">
      <span className="trackInfoTextLabel">{label}</span>
      <span className="trackInfoTextValue">{value}</span>
    </div>
  </div>
));

export {TrackInfoBar};