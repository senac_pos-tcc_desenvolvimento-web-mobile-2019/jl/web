import React from 'react';
import { useTranslation } from "react-i18next";
import {Icon} from '../index';
import {useUsers} from '@growth/commons/context';

import './style.css';

const TechnicalCard = ({createdAt, createdBy, updatedAt, status, estimatedTime, courses, rating }) => {
  const {t} = useTranslation();
  const users = useUsers();
  const author = users.find((user)=>user.id === createdBy);

  return(
    <div className="technicalCardContainer">
      <div className="technicalCardTitle">{t("info.technicalCard")}</div>
      <div className="technicalCardRow">
        <Icon name="id-badge" type="regular" color="var(--color-gray)" size="1x" />
        <div className="technicalCardContent">
          <span className="technicalCardContentTitle">{t("info.author")}</span>
          <span className="technicalCardContentText">{author.name}</span>
        </div>
      </div>
      <div className="technicalCardRow">
        <Icon name="calendar-alt" type="regular" color="var(--color-gray)" size="1x" />
        <div className="technicalCardContent">
          <span className="technicalCardContentTitle">{t("info.createdAt")}</span>
          <span className="technicalCardContentText">{createdAt.toLocaleString()}</span>
        </div>
      </div>
      <div className="technicalCardRow">
        <Icon name="sync" type="solid" color="var(--color-gray)" size="1x" />
        <div className="technicalCardContent">
          <span className="technicalCardContentTitle">{t("info.updatedAt")}</span>
          <span className="technicalCardContentText">{updatedAt.toLocaleString()}</span>
        </div>
      </div>
      {estimatedTime && (
        <div className="technicalCardRow">
          <Icon name="clock" type="regular" color="var(--color-gray)" size="1x" />
          <div className="technicalCardContent">
            <span className="technicalCardContentTitle">{t("info.estimatedTime")}</span>
            <span className="technicalCardContentText">{estimatedTime} {t("info.minutes")}</span>
          </div>
        </div>
      )}
      {courses && (
        <div className="technicalCardRow">
          <Icon name="book-open" type="solid" color="var(--color-gray)" size="1x" />
          <div className="technicalCardContent">
            <span className="technicalCardContentTitle">{t("info.coursesCount")}</span>
            <span className="technicalCardContentText">{courses}</span>
          </div>
        </div>
      )}
      <div className="technicalCardRow">
        <Icon name="minus-circle" type="solid" color="var(--color-gray)" size="1x" />
        <div className="technicalCardContent">
          <span className="technicalCardContentTitle">{t("info.status")}</span>
          <span className="technicalCardContentText">{t(`status.${status}`)}</span>
        </div>
      </div>
      {rating && (
        <div className="technicalCardRow">
          <Icon name="star-half-alt" type="solid" color="var(--color-gray)" size="1x" />
          <div className="technicalCardContent">
            <span className="technicalCardContentTitle">{t("info.evaluation")}</span>
            <span className="technicalCardContentText">{rating}</span>
          </div>
        </div>
      )}
    </div>
  );
};

export {TechnicalCard}