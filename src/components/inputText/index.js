import React from 'react';

import { Icon } from '../Icon';
import './style.css';



const TextInput = ({ className = '', leftIcon, leftIconType, leftIconColor, leftIconSize, rightIcon, rightIconType, rightIconColor, rightIconSize, onRightIconClick, disabled, ...otherProps }) => {
  return (
    <div className={`inputContainer ${className}`} { ...otherProps }>
      {leftIcon && (  
        <Icon className="leftIcon" 
          name={leftIcon}
          type={leftIconType}
          color={leftIconColor }
          size={leftIconSize}
        />
      )}
      <input className="inputTextStyle" disabled={disabled} { ...otherProps }/>
      {rightIcon && (
        <Icon className="rightICon"
          name={rightIcon}
          type={rightIconType}
          color={rightIconColor}
          size={rightIconSize}
          onClick={onRightIconClick}
        />
      )}
    </div>
  );
};

const InputText = ({title, required, legend, rightLegend, value, readonly, short = '', className = '', ...otherProps }) => {
  
  return (
    <div className={`inputTextContainer ${className}`}>
      {title && (  
        <div className="inputTextTitle">
          {title}
          {required && (<span className="inputTextRequired">*</span> )}
        </div>
      )}
      {!readonly && (
        <div className="inputTextBox">
          <input className={`inputTextStyle box ` + short} value={value} { ...otherProps }/>
          {rightLegend && <span >{rightLegend}</span>}
        </div>
      )}
      {readonly && <div className="inputTextStyle readOnly">{value} {rightLegend}</div>}
      {!readonly && legend && (
        <div className="inputTextLegend">
          {legend}
        </div>
      )}
    </div>
  );
};

const TextAreaInput = ({title, required, numRows = 3, legend, value, readonly, className = '', ...otherProps }) => {
  return (
    <div className={`inputTextContainer ${className}`}>
      {title && (  
        <div className="inputTextTitle">{title} 
          {required && (<span className="inputTextRequired">*</span> )}
        </div>
      )}
      {!readonly && <textarea className="inputTextStyle textArea" rows={numRows} value={value} { ...otherProps }/>}
      {readonly && <div className="inputTextStyle readOnly">{value}</div>}
      { legend && (
        <div className="inputTextLegend">
          {legend}
        </div>
      )}
    </div>
  );
};


export { TextInput, InputText, TextAreaInput };
