import React, { useRef, useState, useEffect, useImperativeHandle } from 'react';
import { useTranslation } from "react-i18next";

import { Icon } from '../Icon';

import firebase from "firebase/app";
import 'firebase/storage';
import './style.css';


const IMAGE = 'IMAGE';
const VIDEO = 'VIDEO';

const VALIDATION_TYPES = {
  IMAGE: ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'],
  VIDEO: ['video/mp4', 'video/webm', 'video/ogg'],
};

const fileSize = (size) => {
  if (size === 0) return '0 Bytes';
  const k = 1024;
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
  const i = Math.floor(Math.log(size) / Math.log(k));
  return parseFloat((size / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
};

const validateFile = ({ file, type, maxSize }) =>{
  if (file) {
    const error = new Error();

    if (type) {
      const validTypes = VALIDATION_TYPES[type];
      if (!validTypes || !validTypes.includes(file.type)) {
        error.code = 'invalid_type';
        error.message = 'File type not alllowed';
        throw error;
      }
    }

    if (maxSize) {
      if (file.size > maxSize) {
        error.code = 'size_limit';
        error.message = 'File size is bigger than expected';
        throw error;
      }
    }

    return true;
  }
};

const Dropzone = React.forwardRef(({ placeholder, type, maxSize, previousFileUrl, onValidationError = () => {}, className = '', dropzoneSquare, ...otherProps }, ref) => {
  const {t} = useTranslation();
  const storageRef = firebase.storage().ref();

  const fileInputRef = useRef();
  const imageDisplayRef = useRef();

  const [highlight, setHighlight] = useState(false);
  const [tempFile, setTempFile] = useState();
  const [prevFile, setPrevFile] = useState(previousFileUrl);

  const hasPreview = (!!tempFile || !!(prevFile && prevFile.length));

  useEffect(() => {
    if (tempFile) {
      const reader = new FileReader(); 
      reader.readAsDataURL(tempFile);
      reader.onload = (e) => {
        imageDisplayRef.current.style.backgroundImage = `url(${e.target.result})`;
      };
    } else if (prevFile) {
      imageDisplayRef.current.style.backgroundImage = `url(${prevFile})`;
    } else {
      imageDisplayRef.current.style.backgroundImage = null;
    }
  }, [tempFile, prevFile, imageDisplayRef]);

  const handleTempFile = (file) => {
    try {
      validateFile({ file, type, maxSize });
      setTempFile(file);
      setPrevFile();
    } catch (error) {
      onValidationError(error);
    }
  };

  const handleDrop = (e) => {
    e.preventDefault();
    setHighlight(false);
    const file = e.dataTransfer.files[0];
    handleTempFile(file);
  };

  const handleInputChange = (e) => {
    e.preventDefault();
    const file = e.target.files[0];
    handleTempFile(file);
  };
  
  const handleUpload = async (path) => {
    if (!tempFile && !prevFile) {
      return;
    } if (prevFile) {
      return prevFile;
    } else if (tempFile) {
      const filePath = `${path}.${tempFile.name.split('.').reverse()[0]}`;
      const fileSnapshot = await storageRef.child(filePath).put(tempFile);
      const url = await fileSnapshot.ref.getDownloadURL();
      return url;
    }
  };

  const handleClean =() => {
    fileInputRef.current.value = null;
    setTempFile();
    setPrevFile();
  };

  const handleClick = () => {
    fileInputRef.current.click();
  };

  const handleDragOver = (e) => {
    e.preventDefault();
    setHighlight(true);
  };
  
  const handleDragLeave = (e) => {
    e.preventDefault();
    setHighlight(false);
  };

  const handleDragEnter = (e) => {
    e.preventDefault();
  };

  useImperativeHandle(ref, () => ({
    upload: (path) => handleUpload(path)
  }));

  return (
    <div 
      className={`dropzoneContainer ${className}`} 
      ref={imageDisplayRef}
    >
      <div
        className={`dropzoneArea ${dropzoneSquare ? "dropzoneAreaSquare" : ''} ${highlight ? "dropHighLight" : ""}`}
        onDragOver={handleDragOver}
        onDragLeave={handleDragLeave}
        onDragEnter={handleDragEnter}
        onClick={handleClick}
        onDrop={handleDrop} 
      >
        <input
          ref={fileInputRef}
          className="fileInput"
          type="file"
          onChange={handleInputChange}
        />
        {!hasPreview && <span>{placeholder}</span> }
      </div>

      {hasPreview && (
        <div className="dropzoneClean" onClick={handleClean}>
          <Icon name="trash-alt" size="1x" />
        </div>
      )}
    </div>
  );
});

Dropzone.IMAGE = IMAGE;
Dropzone.VIDEO = VIDEO;

export {Dropzone};