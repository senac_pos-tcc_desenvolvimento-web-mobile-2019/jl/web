import React from 'react';
import { useTranslation } from "react-i18next";
import {Button, TextInput} from '../index';
import './style.css';

const SearchBar = ({buttons = [], legend = '', search, onSearchChange}) => {
  const {t} = useTranslation();
  return(
    <div className="searchBarContainer">
      <div className="searchBarSelectList">
        {legend && <span className="searchBarMargin">{legend}</span>}
        {buttons.map((button, index) => (
          <Button
            key={button.title}
            title={button.title}
            onClick={button.onClick}
            className={`square searchBarMargin ${button.selected ? '' : 'reverse'}`}
          />
        ))}
      </div>
      <div className="searchBarTextInput">
        <TextInput 
          rightIcon="search" rightIconType="solid" 
          rightIconSize="1x" rightIconColor="var(--color-primary)"
          placeholder={t('input.search')}
          value={search}
          onChange={onSearchChange}
        />
      </div>
    </div>
  );
};

export {SearchBar};


