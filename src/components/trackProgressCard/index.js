import React from 'react';
import { useTranslation } from "react-i18next";

import { useTrackCoursesInfo, useUserTrackProgress } from '@growth/commons/context';

import { ProgressBar } from '../index';
import './style.css';


const TrackProgressCard = ({ track, className = '' }) => {
  const { t } = useTranslation();

  const { courses } = useTrackCoursesInfo(track);
  const { percentage, done, status, startedAt, doneAt } = useUserTrackProgress(track);

  return(
    <div className={`trackProgressCardContainer ${className}`}>
      <ProgressBar progress={percentage}/>
      <div className="trackProgressCardText">
        <span>{done.length}/{courses.length}</span>
        <span>{t("info.doneCourses")}</span>
      </div>
      {!!status && (
        <div className="trackProgressCardText">
          {t(`status.${status}`)}
          {status != 'done' && ` desde ${startedAt.toLocaleString()}`}
          {status == 'done' && ` em ${doneAt.toLocaleString()}`}
        </div>
      )}
    </div>
  );
};

export {TrackProgressCard};