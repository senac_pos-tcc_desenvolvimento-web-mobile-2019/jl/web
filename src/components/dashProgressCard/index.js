import React from 'react';
import { useTranslation } from "react-i18next";

import { Constants } from '@growth/commons/utils/';
import { useCurrentUser, useUserCoursesPending, useUserCoursesDone, useUserTracksDone } from '@growth/commons/context';

import { Icon } from '../../components';
import './style.css';


const DashProgressCard = () => {
  const {t} = useTranslation();
  const user = useCurrentUser();
  const coursesPending = useUserCoursesPending();
  const coursesDone = useUserCoursesDone();
  const tracksDone = useUserTracksDone();

  return (
    <div className="dashProgressContainer">
      <div className="column">
        <div className="line">
          <div className="icon"><Icon name="medal" type="solid" size="1x" color="var(--color-gray)" /></div>
          <div className="lineDetail">
              <span>{t('info.courses')}</span>
              <span><span className="heavy">{coursesDone.length}</span> {t('status.completed')}</span>
          </div>
        </div>
        <div className="line">
          <div className="icon"><Icon name="bookmark" type="solid" size="1x" color="var(--color-gray)" /></div>
          <div className="lineDetail">
              <span>{t('info.courses')}</span>
              <span><span className="heavy">{coursesPending.length}</span> {t('status.pending')}</span>
          </div>
        </div>
      </div>
      <div className="column">
        <div className="line">
          <div className="icon"><Icon name="graduation-cap" type="solid" size="1x" color="var(--color-gray)" /></div>
          <div className="lineDetail">
              <span>{t('info.tracks')}</span>
              <span><span className="heavy">{tracksDone.length}</span> {t('status.completed')}</span>
          </div>
        </div>
        <div className="line">
          <div className="icon"><Icon name="calendar-alt" type="regular" size="1x" color="var(--color-gray)" /></div>
          <div className="lineDetail">
              <span>{t('info.lastLogin')}</span>
              <span className="heavy">{user.accessedAt && user.accessedAt.toLocaleDateString()}</span>
          </div>
        </div>
      </div>
    </div>
  );
}

export {DashProgressCard};
