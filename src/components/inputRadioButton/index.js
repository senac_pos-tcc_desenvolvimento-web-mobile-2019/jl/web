import React from 'react';
import {Icon} from '../index';
import './style.css';


const InputRadioButton = ({id, title, className="", rightIcon, rightIconType, rightIconColor, rightIconSize, onRightIconClick,  disabled, editable, ...otherProps}) => {

  return(
    <div className={`radioContainer ${className}`}>
      <label className="radioWrapper">
        <span className="radioInput">
          <input type="radio" name="radio" id={id} {...otherProps} />
          <span className="radioControl"></span>
        </span>
        <label htmlFor={id} className="radioLabel">{title}</label>
      </label>
      {rightIcon && (
        <Icon className="radioRightIcon"
          name={rightIcon}
          type={rightIconType}
          color={rightIconColor}
          size={rightIconSize}
          onClick={onRightIconClick}
        />
      )}
    </div>

  );
}

export {InputRadioButton};

