import React, {useMemo} from 'react';
import { Strings } from '@growth/commons/utils';

import './style.css';

const Avatar = React.memo(({ size = 60, color = "var(--color-primary)", url, string, classname='', ...otherProps }) => {
  const initials = useMemo(() => (
    string ? Strings.initials(string) : ''
  ), [string]);

  const style = {
    height: `${size}px`,
    width: `${size}px`
  };

  const initialsStyle = useMemo(() => ({ 
    color,
    fontSize: size * 0.5,
  }), [color, size]);

  return (
    <div className={`avatar ${classname}`} style={style} { ...otherProps }>
      {!!url ? (
        <div className="image" style={{backgroundImage: `url(${url})`}} /> 
        
      ) : (
        <div className="initials" style={initialsStyle}>{initials}</div>
      )}
    </div>
  );
});

export {Avatar}