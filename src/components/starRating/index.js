import React, {useState} from 'react';
import {Icon} from '../index';

import './style.css';

const StarRating = ({ value, onClick = () => {} }) => {
  const[rating, setRating] = useState(value);
  const[hover, setHover]= useState(null);

  return (
    <div className="starRatingContainer">
      {[...Array(5)].map((star, index) => {
        const ratingValue = index + 1;
        return (
          <label>
            <input type="radio" name="rating" value={ratingValue} onClick={() => setRating(ratingValue)} />
            <Icon
              name='star'
              color={`var(--color-${ratingValue <= (hover || rating) ? 'gold' : 'black-light'})`}
              type='solid'
              size="2x"
              className="star"
              onMouseEnter={()=> setHover(ratingValue)}
              onMouseLeave={()=> setHover(null)}
              onClick={() => onClick(ratingValue)}
            />
          </label>
        )
      })}
      
    </div>
  );
}

export {StarRating};