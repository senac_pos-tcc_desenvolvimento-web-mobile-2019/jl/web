import React from 'react';
import { useTranslation } from "react-i18next";
import { useTrackCoursesInfo, useUserTrackProgress } from '@growth/commons/context';
import badge from '../../assets/LogoScout.png';
import {ProgressBar, Icon, Badge} from '../index';
import './style.css';


const TrackCard = ({track, onClick}) => {
  const {t} = useTranslation();

  const { courses, estimatedTime } = useTrackCoursesInfo(track);
  const trackProgress = useUserTrackProgress(track);

  return(
    <div className="trackCardContainer" onClick={onClick}>
      <div className="trackCardLeft">
        <Badge url={track.badgeUrl } size={60} />
      </div>
      <div className="trackCardMiddle">{t('title.track')}
        <div className="trackCardMiddleTitle">{track.name}</div>
        <ProgressBar progress={trackProgress && trackProgress.percentage}/>
        <small>
          <div>{trackProgress.pending.length} em andamento, {trackProgress.done.length} concluidos</div>
          {!!trackProgress.status && (
            <div>
              {t(`status.${trackProgress.status}`)}
              {trackProgress.status != 'done' && ` desde ${trackProgress.startedAt.toLocaleString()}`}
              {trackProgress.status == 'done' && ` em ${trackProgress.doneAt.toLocaleString()}`}
            </div>
          )}
        </small>
        <div className="trackCardDescription">{track.description}</div>
      </div>
      <div className="trackCardRight">
        <div className="trackCardGreen">
          <span className="trackCardGreenTitle">{t("info.totalEstimatedTime")}</span>
          <span>{estimatedTime} {t("info.minutes")}</span>
        </div>
        <div className="trackCardGreen">
          <span className="trackCardGreenTitle">{t("info.avalibleCourses")}</span>
          <span>{courses.length}</span>
        </div>
      </div>
    </div>
  );
};

const TrackCardMini = ({track, onClick}) => {

  const { t } = useTranslation();
  const { courses, estimatedTime } = useTrackCoursesInfo(track);
  const trackProgress = useUserTrackProgress(track);

  return(
    <div className="trackCardMiniContainer" onClick={onClick}>
      <div className="trackCardMiniWrapper">
        <span className="track">{t("title.track")}</span>
        
        {trackProgress && trackProgress.status && (
          <Icon
            name={(trackProgress.status == 'done' ? 'graduation-cap' : 'bookmark')}
            color={`var(--color-${trackProgress.status == 'done' ? 'primary' : 'black-light'})`}
            type="solid"
            size="2x"
            className="trackCardMiniIcon"
          />
        )}
      
        <Badge url={track.badgeUrl } size={80} className="trackCardMinibadge"/>
        <span className="trackTitle">{track.name}</span>
        <div className="lineBox">
          <div className="line">
            <div className="icon"><Icon name="calendar-alt" type="regular" size="1x" color="var(--color-gray)" /></div>
            <div className="lineDetail">
                <span>{t('info.updatedAt')}</span>
                <span className="heavy"> {"20/10/2020"}</span>
            </div>
          </div>
          <div className="line">
            <div className="icon"><Icon name="book-open" type="solid" size="1x" color="var(--color-gray)" /></div>
            <div className="lineDetail">
                <span><span className="heavy"> {trackProgress.pending.length}</span> {t('status.pending')}</span>
                <span><span className="heavy"> {trackProgress.done.length}</span> {t('status.completed')}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export {TrackCard, TrackCardMini};