import React from 'react';
import './style.css';

const ProgressBar = ({progress, heightDiv=15}) => {
  return(
    <div className="progressBarContainer" style={{height: `${heightDiv}px`}}>
      <div className="progressBarStatus" style={{width: `${progress}%`}}></div>
    </div>
  );
};

export {ProgressBar};

