import React from 'react';

import './style.css';


const PageHeader = ({ badge, title, description, className = '', children, ...otherProps }) => {
  return(
    <div className={`pageHeaderContainer ${className}`} { ...otherProps }>
      <div className="pageHeaderContent">
        { !!(badge && badge.length) && <img src={badge} className="pageHeaderBadge" /> }
        <div className="pageHeaderText">
          { !!(title && title.length) && <div className="title">{title}</div> }
          { !!(description && description.length) && <div className="description">{description}</div> }
        </div>
      </div>
      <div className="pageHeaderChildren">
        { children }
      </div>
    </div>
  );
};

export { PageHeader };