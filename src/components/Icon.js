import React, {useMemo} from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';


library.add(far, fas, fab);

const Icon = React.memo(({ name, type = 'solid', color, size = "1x", onClick, ...otherProps }) => {
  const iconPackage = useMemo(() => type === 'brands' ? 'fab' : type === 'regular' ? 'far' : 'fas', [type]);

  return (
    <div onClick={onClick}>
      <FontAwesomeIcon icon={[iconPackage, name]} color={color} size={size} { ...otherProps } />
    </div>
    
  );
});

export {Icon};
