import React from 'react';

import { InputCheckBox } from '../index';
import './style.css';


const InputCheckboxGroup = React.memo(({ title, required, data = [], isSelected, onChange, disabled, readonly, className="", ...otherProps}) => (
  <div className={`inputCheckboxGroupContainer ${className}`}>
    {title && (
      <div className="inputCheckboxGroupTitle">
        {title}
        {required && (<span className="required">*</span> )}
      </div>
    )}
    {!readonly && (
      <div className="inputCheckboxGroupInputs">
        {data.map((checkboxData, index, allData) => {
          return (
            <InputCheckBox
              key={checkboxData.value}
              id={checkboxData.value}
              checked={isSelected ? isSelected(checkboxData, index, allData) : checkboxData.selected}
              onChange={onChange}
              { ...checkboxData }
            />
          );
        })}
      </div>
    )}
    {!!readonly && (
      <div className="inputCheckboxGroupReadOnly">
        {
          data.filter((checkboxData, index, allData) => isSelected ? isSelected(checkboxData, index, allData) : checkboxData.selected)
          .map(({ title }) => title)
          .join(', ')
        }
      </div>
    )}
  </div>
));

export {InputCheckboxGroup};

