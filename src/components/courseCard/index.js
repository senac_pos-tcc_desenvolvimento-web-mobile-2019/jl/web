import React from 'react';
import { useTranslation } from "react-i18next";
import { useUserCourseProgress } from '@growth/commons/context';
import {Icon, Badge} from '../index';

import './style.css';

const CourseCard = ({ course, onClick }) => {

  const { t } = useTranslation();
  const courseProgress = useUserCourseProgress(course);

  return(
    <div className="courseCardContainer" onClick={onClick}>
      <div className="courseCardheader">
        <span>Treinamento</span>
        {courseProgress && courseProgress.status && (
          <Icon
            name={(courseProgress.status == 'done' ? 'medal' : 'bookmark')}
            color={`var(--color-${courseProgress.status == 'done' ? 'primary' : 'black-light'})`}
            type="solid"
            size="2x"
            className="courseCardIcon"
          />
        )}
      </div>
      <div className="courseCardContent">
        <div className="courseCardLogo">
          <Badge url={course.badgeUrl } />
        </div>
        <div className="courseCardtexts">
          <div className="courseCardTitle">{course.name}</div>
          <div>{course.description}</div>
          <div className="courseCardStatus">
            {courseProgress && courseProgress.status && t(`status.${courseProgress.status}`)}
            <small>
              {courseProgress && courseProgress.status == 'pending' && ` desde ${courseProgress.startedAt.toLocaleString()}`}
              {courseProgress && courseProgress.status == 'done' && ` em ${courseProgress.doneAt.toLocaleString()}`}
            </small>
          </div>
        </div>
      </div> 
    </div>
  );
}

const CourseCardMini = ({course, onClick}) => {

  const { t } = useTranslation();
  const courseProgress = useUserCourseProgress(course);
  return(
    <div className="courseCardMiniContainer" onClick={onClick}>
      <div className="courseCardMiniWrapper">
        <span className="course">{t("title.course")}</span>
        {courseProgress && courseProgress.status && (
          <Icon
            name={(courseProgress.status == 'done' ? 'medal' : 'bookmark')}
            color={`var(--color-${courseProgress.status == 'done' ? 'primary' : 'black-light'})`}
            type="solid"
            size="2x"
            className="courseCardMiniIcon"/>
        )}
        <Badge url={course.badgeUrl } />
        <span className="courseTitle">{course.name}</span>
        <div className="line">
          <div className="icon"><Icon name="calendar-alt" type="regular" size="1x" color="var(--color-gray)" /></div>
          <div className="lineDetail">
              <span>{t('info.updatedAt')}</span>
              <span className="heavy"> {course.date.toLocaleDateString()}</span>
          </div>
        </div>
      </div>
    </div>
  );
}



export {CourseCard, CourseCardMini};