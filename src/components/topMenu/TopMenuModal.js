import React from 'react';
import {Icon} from '../../components';

import Firebase from '@growth/commons/firebase';
import { useTranslation } from "react-i18next";
import {NavLink} from 'react-router-dom';
import './styleTopMenuModal.css';

const TopMenuModal = ({ className = '' }) => {
  const {t} = useTranslation();

  return (
    <div className={'topMenuModalContainer' + ' ' + className}>
      <ul className='navContainer'>
        <TopMenuLink to="/profile" icon="user-alt" label={t('menu.myProfile')} />
        <TopMenuLink to={{ pathname: '/courses', state: 'my' }} icon="book-open" label={t('menu.myCourses')} />
        <TopMenuLink to={{ pathname: '/tracks', state: 'my' }} icon="layer-group" label={t('menu.myTracks')} />
        {/*<TopMenuLink to="/certificate" icon="award" label={t('menu.myCertificates')} />*/}
        <hr />
        <TopMenuLink to="#" onClick={() => Firebase.auth().signOut()} icon="sign-out-alt" label={t('menu.signOut')} />
      </ul>
    </div>
  );
};

const TopMenuLink = React.memo(({ to, onClick, icon, label }) => (
  <NavLink to={to} onClick={onClick}>
    <li className='navItem'>
      <div className="navIcon"><Icon name={icon} type="solid" size="1x" color="var(--color-primary)" /></div>
      <span className='navText'>{label}</span>
    </li>
  </NavLink>
));

export default TopMenuModal;