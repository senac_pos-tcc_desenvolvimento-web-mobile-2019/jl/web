import React, {useState, useRef} from 'react';
import { useTranslation } from "react-i18next";
import { useCurrentUser } from '@growth/commons/context';
import {Icon, Avatar} from '../../components';
import {useOutsideClick} from '../../utils';
import TopMenuModal from './TopMenuModal';
import './style.css';

const TopMenu = ({ title, subtitle }) => {
  const {t} = useTranslation();
  const user = useCurrentUser();

  const [isModalVisible, setIsModalVisible] = useState(false);
  const ref = useRef();

  useOutsideClick(ref, ()=>{
    if(isModalVisible) setIsModalVisible(false);
  });

  return(
    <div className="topMenuContainer">
      <div className="titleContainer">
        {!!(title && title.length) && <span className="title">{title}</span>}
        {!!(subtitle && subtitle.length) && <span className="subtitle">{subtitle}</span>}
      </div>

      <div className="nameUser">
        <span className="name">{user.name} </span>
        <span className="role">({t(`roles.${user.role}`)})</span>
      </div>

      <div className="profileContainer" onClick={() => setIsModalVisible(true)}>
        <Avatar url={user.avatarUrl} string={user.name} classname="avatarTopMenu" />
        
        <Icon name="chevron-down" type="solid" size="1x" color="var(--color-gray)" />

        <div ref={ref}> 
          <TopMenuModal className={!isModalVisible ? 'hidden' : 'active'}  />
        </div>
      </div>
    </div>
  );
}

export {TopMenu};