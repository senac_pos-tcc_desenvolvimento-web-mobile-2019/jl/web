import React from 'react';

import { InputRadioButton } from '../index';
import './style.css';


const InputRadioGroup = React.memo(({ title, required, data = [], isSelected, onChange, disabled, readonly, className="", ...otherProps}) => {
  const selected = data.find((radioData, index, allData) => isSelected ? isSelected(radioData, index, allData) : radioData.selected);

  return (
    <div className={`inputRadioGroupContainer ${className}`}>
      {title && (
        <div className="inputRadioGroupTitle">
          {title}
          {required && (<span className="required">*</span> )}
        </div>
      )}
      {!readonly && (
        <div className="inputRadioGroupInputs">
          {data.map((radioData, index, allData) => {
            return (
              <InputRadioButton
                key={radioData.value}
                id={radioData.value}
                onChange={onChange}
                checked={radioData == selected}
                rightIconSize="sm"
                disabled={disabled}
                { ...radioData }
              />
            );
          })}
        </div>
      )}
      {!!readonly && !!selected && (<div className="inputRadioGroupReadOnly">{selected.title}</div>)}
    </div>
  );
});

export {InputRadioGroup};

