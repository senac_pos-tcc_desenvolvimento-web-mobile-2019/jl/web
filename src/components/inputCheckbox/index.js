import React from 'react';

import './style.css'


const InputCheckBox = ({id, title, className="", disabled, editable, ...otherProps}) => {

  return(
      <label className={`checkboxWrapper ${className}`}>
        <span className="checkboxInput">
          <input type="checkbox" id={id} { ...otherProps } />
          <span className="checkboxControl"></span>
        </span>
      <label htmlFor={id} className="checkboxLabel">{title}</label>
    </label>
  );
}

export {InputCheckBox};