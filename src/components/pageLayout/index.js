import React, { useImperativeHandle } from 'react';

import { SideMenu, TopMenu, ScreenLoader } from '../index';
import './style.css';


const PageLayout = React.memo(React.forwardRef(({ title, subtitle, className = '', renderHeader, renderStickyHeader, renderFooter, renderStickyFooter, loading, children, ...otherProps }, ref) => {
  useImperativeHandle(ref, () => ({
    scrollToTop: () => {
      document.querySelector('.pageContentWrapper').scrollTop = 0;
    }
  }));

  return(
    <div className="pageContainer">
      <SideMenu />
      <div className="pageContentWrapper">
        <div className="pageContentHeaderContainer">
          <TopMenu title={title} subtitle={subtitle} />
          { !!renderStickyHeader && renderStickyHeader() }
        </div>

        { !!renderHeader && renderHeader() }
        <div className={`pageContentContiner ${className}`}>
          {children}
        </div>
        { !!renderFooter && renderFooter() }

        <div className="pageContentFooterContainer">
          { !!renderStickyFooter && renderStickyFooter() }
        </div>
      </div>

      <ScreenLoader visible={loading} />
    </div>
  );
}));

export {PageLayout};