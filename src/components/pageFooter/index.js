import React from 'react';

import './style.css';


const PageFooter = ({ className = '', children, ...otherProps }) => {
  return(
    <div className={`pageFooterContainer ${className}`} { ...otherProps }>
      { children }
    </div>
  );
};

export { PageFooter };