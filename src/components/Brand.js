import React, { useMemo } from 'react';
import {ReactComponent as Icon} from '../assets/brandIcon.svg';
import {ReactComponent as IconText} from '../assets/brandText.svg';

const BrandIcon = ({ color = 'var(--color-white)', size = 100, style, ...otherProps }) => {
  const brandClassName = useMemo(() => ({ width: size, height: size, fill: color, stroke: color, ...style }), [size, color, style]);
  return (
    <div { ...otherProps }>
        <Icon style={brandClassName} />
    </div>
  );
};

const BrandText = ({ color = "var(--color-white)", size = 100, style, ...otherProps }) => {
const brandClassName = useMemo(() => ({ width: size, height: size, fill: color, stroke: color, ...style }), [size, color, style]);
  return (
    <div { ...otherProps }>
      <IconText style={brandClassName} />
    </div>
  );
};

export {
  BrandIcon,
  BrandText
};
