import React from 'react';
import './style.css';



const TitleBottomLine = ({title}) => {
  return(
    <div className="headerWithBottomLine">{title}</div>
  );
}

export {TitleBottomLine};