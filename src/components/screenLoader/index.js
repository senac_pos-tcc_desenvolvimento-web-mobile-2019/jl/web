import React from 'react';

import './style.css';


const ScreenLoader = ({ visible = false, ...otherProps }) => {
	return(
		<div className={`screenLoader ${visible ? 'visible' : ''}`}>
			<div className="loader"/>
		</div>
	);
}

export {ScreenLoader};