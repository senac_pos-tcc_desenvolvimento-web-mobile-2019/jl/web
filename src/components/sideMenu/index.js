import React from 'react';
import { useTranslation } from "react-i18next";
import {NavLink} from 'react-router-dom';
import {Icon, BrandIcon, BrandText} from '../../components';
import { useCurrentUser } from '@growth/commons/context';
import './style.css';


const SideMenu = (props) => {
  const {t} = useTranslation();
  const user = useCurrentUser();

  return(
    <div className='sideMenuContainer'>
      <div className="brandContainer">
        <BrandIcon className='brandIconStyle' />
        <BrandText className='brandTextStyle' />
      </div>

      <ul className='navContainer'>
        <SideMenuLink to="/dashboard" icon="home" label={t('menu.dashboard')} />
        <SideMenuLink to="/courses" icon="book-open" label={t('menu.courses')} />
        <SideMenuLink to="/tracks" icon="layer-group" label={t('menu.tracks')} />
        {/*
        <SideMenuLink to="/certificate" icon="award" label={t('menu.certificate')} />
        <SideMenuLink to="/ranking" icon="flag-checkered" label={t('menu.ranking')} />
        */}
      </ul>

      {user.role == 'admin' && (
        <React.Fragment>
          <div className="admContainerHeader">{t('menu.admArea')}</div>
          <ul className='admContainer'>
            <SideMenuLink to="/adminUsers" icon="user-edit" label={t('menu.users')} />
            <SideMenuLink to="/adminCourses" icon="edit" label={t('menu.courses')} />
            <SideMenuLink to="/adminTracks" icon="edit" label={t('menu.tracks')} />
            <SideMenuLink to="/framework" icon="code" label={t('menu.frame')} />
          </ul>
        </React.Fragment>
      )}
    </div>
  );
};

const SideMenuLink = React.memo(({ to, icon, label }) => (
  <NavLink to={to} >
    <li className='navItem'>
      <div className="navIcon"><Icon name={icon} type="solid" size="1x" color="var(--color-white)" /></div>
      <span className='navText'>{label}</span>
    </li>
  </NavLink>
));

export {SideMenu};