
const phoneMask = (value) => {
  return value
    .replace(/\D/g, '') 
    .replace(/(\d{2})(\d)/, '($1)$2')
    .replace(/(\d{9})\d+?$/, '$1');
};

const dateMask = (value) => {
  return value
    .replace(/\D/g, '') 
    .replace(/(\d{2})(\d)/, '$1/$2')
    .replace(/(\d{2})(\d)/, '$1/$2')
    .replace(/(\d{4})\d+?$/, '$1');
};

export {phoneMask, dateMask}