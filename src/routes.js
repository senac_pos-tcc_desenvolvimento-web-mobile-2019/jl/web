import React, { useMemo } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import {Layout} from './components';
import {Framework, SignIn, Tracks, Track,
        Profile, Dashboard, Courses, Course,
        ForgotPwd, CreateAccount, Error,
        AdminCourses, AdminTracks, AdminUsers,
        ManageCourse, ManageTrack
      } from './screens';

import { useStatus, useCurrentUser } from '@growth/commons/context';


const Routes = () => {
  const status = useStatus();
  const currentUser = useCurrentUser();

  const initializing = useMemo(() => status != 'ready', [status]);
  const authenticated = useMemo(() => status == 'ready' && !!currentUser, [status, currentUser]);

  if (initializing)
    return null;
  else
    return (
      <BrowserRouter>
        <Switch>
          {!authenticated && <Route render = {(props) => (
            <React.Fragment>
              <Route exact path="/" component={SignIn} />
              <Route path="/signIn" component={SignIn} />
              <Route path="/forgotPwd" component={ForgotPwd} />
              <Route path="/createAccount" component={CreateAccount} />
              <Route exact path="/*">
                <Redirect to="/" />
              </Route>
            </React.Fragment>
          )}/>}
          {authenticated && <Route render = {(props) => (
            <React.Fragment>
              <Route exact path="/" component={Dashboard} />
              <Route path="/profile" component={Profile} />
              <Route path="/dashboard" component={Dashboard} />
              <Route exact path="/courses" component={Courses} />
              <Route path="/courses/:id" component={Course} />
              <Route exact path="/tracks" component={Tracks} />
              <Route path="/tracks/:id" component={Track} />
              <AdminRoute path="/framework" component={Framework} />
              <AdminRoute path="/adminUsers" component={AdminUsers} />
              <AdminRoute exact path="/adminCourses" component={AdminCourses} />
              <AdminRoute exact path="/adminCourses/manage" component={ManageCourse} />
              <AdminRoute path="/adminCourses/manage/:id" component={ManageCourse} />
              <AdminRoute exact path="/adminTracks" component={AdminTracks} />
              <AdminRoute exact path="/adminTracks/manage" component={ManageTrack} />
              <AdminRoute path="/adminTracks/manage/:id" component={ManageTrack} />
              <Route exact path="/*">
                <Redirect to="/" />
              </Route>
            </React.Fragment>
          )}/>}
        </Switch>
      </BrowserRouter>
    );
};

const AdminRoute = ({ component: Component, ...otherProps }) => {
  const currentUser = useCurrentUser();
  const isAdmin = useMemo(() => (!!currentUser && currentUser.role == 'admin'), [currentUser]);

  return (
    <Route { ...otherProps }
      render={props =>
        isAdmin ? (<Component { ...props } />) : (<Redirect to="/" />)
      }
    />
  );
};

export default Routes;
